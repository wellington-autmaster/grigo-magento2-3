<?php


/**
 * Todas as rotas devem ter prefixo 'admin.'.
 */

Route::group(['namespace' => 'Marktplace\Produto'], function () {

    /** Pesquisar e editar produtos do SSPlus */ 
     Route::get('produtos/', ['uses' => 'SSProdutosController@produtosPesquisa', 'as' =>'SSprodutosPesquisa' ]);
     Route::get('produtos/editar/{codigo}', ['uses' => 'SSProdutosController@SSProdutosEditar', 'as' =>'SSProdutosEditar' ]);
    /*************************************/

    /** Cadastra veiculos nos produtos */
     Route::get('produtos/anos/show/{produto}', 'ProdutoController@getAnosCadastrados')->name('produto.getAnosCadastrados');
     Route::post('produtos/cadastro/veiculo', 'ProdutoController@cadastraVeiculo')->name('produto.cadastraVeiculo');
     Route::get('produtos/remove/veiculo/{idano}/{idproduto}', 'ProdutoController@deleteVeiculo')->name('produto.deleteVeiculo');
    /*************************************/

    /**  Selecionar veiculos nos produtos **/
     Route::get('veiculos/getMarcas/{tipo}', 'VeiculosController@getMarcas')->name('veiculos.getMarcas');
     Route::get('veiculos/getVeiculos/{marca}', 'VeiculosController@getVeiculos')->name('veiculos.getVeiculos');
     Route::get('veiculos/getAnos/{veiculo}/{produto}', 'VeiculosController@getAnos')->name('veiculos.getAnos');
    /*************************************/
    
    /** Caracteristicas dos produtos **/
     Route::get('/caracteristicas/pesquisa/{descricao}', 'PodutoCaracteristicaController@getCaracteristicas')->name('produto.getCaracteristicas');
     Route::post('/produto/caracteristica', 'PodutoCaracteristicaController@CadastraCaracteristica')->name('produto.CadastraCaracteristica');
     Route::get('/produto/caracteristica/cadastradas/{ssproduto}', 'PodutoCaracteristicaController@getCaracteristicasProdutos')->name('produto.getCaracteristicasProdutos');
     Route::get('/produto/caracteristica/delete/{caracteristica}', 'PodutoCaracteristicaController@deletaCaracteristicaProduto')->name('produto.deletaCaracteristicaProduto');
     /*************************************/
});