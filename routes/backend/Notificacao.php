<?php


/**
 * Todas as rotas devem ter prefixo 'admin.'.
 */

Route::group(['namespace' => 'Notificacao'], function () {

    Route::get('notificacoes','NotificacaoController@notificacoes')->name('notificacoes');


});

