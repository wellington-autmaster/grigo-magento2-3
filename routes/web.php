<?php


/**
 * Global Routes
 * Routes that are used between both frontend and backend.
 */

// Switch between the included languages
Route::get('lang/{lang}', 'LanguageController@swap');

Route::get('img/{produto}', 'Backend\Marktplace\Produto\ImgSSplusController@ProdutosImagem');

Route::get('imagem/produto/{empresa}/{foto}/{produto}' ,
'Backend\Marktplace\Produto\SSProdutosController@getImagemProduto')->name('getImg');

Route::get('imagem/produto/{foto}/{produto}/' ,
    'Backend\Marktplace\Produto\SSProdutosController@ImagemProduto')->name('ImgProduto');

Route::get('pedidos', 'Backend\Marktplace\OcPedidosController@getPedidos')->name('pedidos');

Route::get('/pedidos_monitor', function () {
    return view('frontend.monitor_pedidos');
});

Route::get('/status-contrato', function () {
    $empresa = \App\Models\Marktplace\Marktplace::where('empresa', '0001')->first();
    $teste = \App\MasterProcess::statusContrato($empresa, 'Opencart');

    return $teste;
});




/*
 * Frontend Routes
 * Namespaces indicate folder structure
 */
Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {
    include_route_files(__DIR__.'/frontend/');
});

/*
 * Backend Routes
 * Namespaces indicate folder structure
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    /*
     * These routes need view-backend permission
     * (good if you want to allow more than one group in the backend,
     * then limit the backend features by different roles or permissions)
     *
     * Note: Administrator has all permissions so you do not have to specify the administrator role everywhere.
     * These routes can not be hit if the password is expired
     */
    include_route_files(__DIR__.'/backend/');

});
