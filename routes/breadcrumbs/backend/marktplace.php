<?php



/*
                        Configuração de Breadcrumbs
 Para cada view se cria um Breadcrumbs, deve-se utilizar o mesmo apelido das rotas.
 Utilizar o Modo $breadcrumbs->Parent para informar que esta dentro de um grupo, exemplo:
 Editar produto vem depois da Pesquisa de produtos, então o parente direto é a pesquisa de produtos.
    $breadcrumbs->Parent('admin.SSprodutosPesquisa');
 Ficaria> Dashboard>>Pesquisa de produtos>>Editar Produtos

*/


Breadcrumbs::register('admin.marktplace', function ($breadcrumbs) {
$breadcrumbs->Parent('admin.dashboard');
$breadcrumbs->push('<i class=\'fa fa-cart-plus\' aria-hidden=\'true\'></i>  Marktplaces', route('admin.marktplace'));

});

Breadcrumbs::register('marktplace.update', function ($breadcrumbs) {
$breadcrumbs->Parent('admin.marktplace');
$breadcrumbs->push('Atualizar', route('admin.marktplace.update'));
});

Breadcrumbs::register('admin.marktplace.editar', function ($breadcrumbs) {
$breadcrumbs->Parent('admin.marktplace');
$breadcrumbs->push('<i class=\'fa fa-pencil-square-o\' aria-hidden=\'true\'></i> Editar', route('admin.marktplace.editar',1));

});


//PRODUTOS

Breadcrumbs::register('admin.SSprodutosPesquisa', function ($breadcrumbs) {
    $breadcrumbs->Parent('admin.dashboard');
    $breadcrumbs->push('<i class=\'fa fa-search\' aria-hidden=\'true\'></i>  Pesquisa de Produtos', route('admin.SSprodutosPesquisa'));
});

Breadcrumbs::register('admin.SSProdutosEditar', function ($breadcrumbs) {
 //   $breadcrumbs->Parent('admin.dashboard');
    $breadcrumbs->Parent('admin.SSprodutosPesquisa');
    $breadcrumbs->push('<i class=\'fa fa-pencil-square-o\' aria-hidden=\'true\'></i>  Editar Produto', route('admin.SSProdutosEditar','0'));
});