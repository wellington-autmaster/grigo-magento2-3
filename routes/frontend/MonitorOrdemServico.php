<?php


Route::group(['namespace' => 'MonitorOrdemServico'], function () {
    Route::get('monitor/getordens/{empresa}', 'MonitorOrdemServicoController@getOrdens')->name('servicos');
    Route::get('monitor/servico/{empresa}', 'MonitorOrdemServicoController@OrdenServicos')->name('viewservicos');

});