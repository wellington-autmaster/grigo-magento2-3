<?php

use Illuminate\Database\Seeder;
use App\Models\Empresa\Empresa;
use App\Models\Monitor\Monitor;
use Illuminate\Support\Facades\DB;

class SeedMonitor extends Seeder
{
    use DisableForeignKeys;
    /**
     * Run the database seeds.
     * Cria uma monitor de senha preconfigurado para cada empresa, todos desativados.
     *
     * @return void
     */
    public function run()
    {

        if ($this->command->confirm('Deseja recriar os monitores de senhas?'))
        {
            $this->disableForeignKeys();

            Monitor::truncate();

            $empresas = Empresa::all();

            foreach($empresas as $empresa)
            {
                Monitor::create(
                    [
                        'empresa_id' => $empresa->id,
                        'empresa' => $empresa->codigo,
                        'canal' => $empresa->cgc .'-'.$empresa->codigo.'-moitorSenhas',
                        'canalPromocao' => $empresa->cgc .'-'.$empresa->codigo.'-promocao',
                        'promocoes' => 1,
                        'senhas' => 1,
                        'modoentregabalcao' => true,
                        'headerColor' => '#1b1b1b',
                        'headerTextColor' => '#dadada',
                        'proDescSize' => '45',
                        'proDescColor' => '#c7c7c7',
                        'proValSize' => '80',
                        'proValColor' => '#19ae19',
                        'qtdSenhas' => 5,
                        'qtdsenhasaguardando' => 10,
                        'conferencia' => false,
                        'imgBackground' => '/img/frontend/monitor/backupground_monitor.jpeg',
                        'imgLogo' => '',
                        'status' => 1,
                        'noticias' => 0,
                        'noticias_fonte' => 'globo',

                    ]);
            };


            $this->enableForeignKeys();
        }

    }
}
