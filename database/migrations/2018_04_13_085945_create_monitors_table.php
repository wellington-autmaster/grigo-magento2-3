<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monitors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_id')->unsigned()->comment('Codigo de relacionamento com a tabela empresas');
            $table->string('empresa',4)->comment('Codigo da empresa no SSPlus(empfil');
            $table->string('canal')->comment('Canal privado do monitor para o Pusher. CNPJ-CodEmpresa-moitorSenhas');
            $table->string('canalPromocao')->comment('Canal para pusher enviar promoções');
            $table->boolean('promocoes')->nullable()->comment('Ativar ou Desativar a exibição das promoções no monitor');
            $table->boolean('senhas')->nullable()->comment('Ativar ou Desarivar a exibição de senhas no monitor');
            $table->boolean('modoentregabalcao')->default(true)->comment('Se true, exibe apenas as separações para retirada no balcão');
            $table->string('headerColor')->comment('Cor do Header no monitor');
            $table->string('headerTextColor')->comment('Cor do texto do Header no monitor');
            $table->string('proDescSize')->comment('Tamanho da fonte da descrição de produtos das promoções no Monitor');
            $table->string('proDescColor')->comment('Cor da descrição de produtos das promoções');
            $table->string('proValSize')->comment('Tamanho da fonte que exibe o preco dos proddutos em promoção');
            $table->string('proValColor')->comment('Cor da fonte do valor do produto em promoção');
            $table->integer('qtdSenhas')->nullable()->comment('Quantidade de senhas para exibir no monitor');
            $table->string('imgBackground')->nullable()->comment('Imagem de fundo do monitor');
            $table->string('imgLogo')->nullable()->comment('Imagem do logotipo da empresa para exibir no monitor');
            $table->boolean('status')->comment('Ativa ou Desativa o monitor');
            $table->boolean('noticias')->comment('Ativa ou Desativa o componente de noticias');
            $table->string('noticias_fonte')->comment('Fonte de noticias a exibir no painel');

            $table->timestamps();

            $table->foreign('empresa_id')->references('id')->on('empresas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monitors');
    }
}
