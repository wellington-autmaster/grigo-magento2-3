<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonitorPromocaosTable extends Migration
{
    /**
     * Run the migrations.
     * View criada que mostra promoções gerais (sem clientes vinculados e tbm a imagem do produto, caso
     * exista.
     * @return void
     */
    public function up()
    {
        DB::connection('pgsql')->statement("
         CREATE OR REPLACE VIEW view_aut_promocao AS SELECT 
                promocoesproduto.codigopromocao, promocoesproduto.item,pccdite0.descr1, promocoesproduto.prvist, 
                promocoesproduto.prpromo, promocoesproduto.dtinicio,promocoesproduto.dtfinal,promocoesproduto.qtminima, 
                promocoesproduto.qtestlimite, promocoesproduto.tipo_preco, promocoesproduto.ativa, promocoesproduto.codtabpre, 
                promocoesproduto.opid
           FROM promocoesproduto

	            LEFT JOIN pccdite0 ON promocoesproduto.item = pccdite0.codigo 
           where 
                promocoesproduto.codigopromocao NOT IN (SELECT cliente_promocao.codigopromocao FROM cliente_promocao) 
                and promocoesproduto.ativa = 't' and promocoesproduto.dtfinal > current_date
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::connection('pgsql')->statement('DROP VIEW IF EXISTS view_aut_promocao');
    }
}
