<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modulos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('empresa',4);
            $table->string('descricao');
            $table->boolean('status')->default(false);
            $table->boolean('producao')->default(false);
            $table->boolean('cancelado')->default(false);
            $table->date('data_ativacao')->nullable();
            $table->date('data_producao')->nullable();
            $table->date('data_cancelamento')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modulos');
    }
}
