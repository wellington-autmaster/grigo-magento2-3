<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('empresa_id')->default(1)->unsigned();
            $table->string('empresa_codigo')->default('0001');

            $table->foreign('empresa_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('users', function (Blueprint $table) {
        $table->dropForeign('users_empresa_id_foreign');
        $table->dropColumn('empresa_id');
        $table->dropColumn('empresa_codigo');
    });
    }
}
