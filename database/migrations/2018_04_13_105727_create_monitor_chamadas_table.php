<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonitorChamadasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monitor_chamadas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('empresa',4);
            $table->string('planil',8);
            $table->date('data');
            $table->time('hora_inicio')->nullable($value = true)->default('00:00:00')->comments('Inicio da separação');
            $table->time('hora_fim')->nullable($value = true)->default('00:00:00')->comments('Separação concluida');
            $table->integer('codigo');
            $table->string('clifor',8);
            $table->string('descri',60);
            $table->string('nronff',6);
            $table->integer('modoentregabalcao')->default('3')->comments('2 = Entrega. 3 = Retira do balcão');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monitor_chamadas');
    }
}
