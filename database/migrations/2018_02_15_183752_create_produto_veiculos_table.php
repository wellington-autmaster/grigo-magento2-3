<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutoVeiculosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produto_veiculos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ssproduto_id');
            $table->integer('tipo_id')->unsigned();
            $table->integer('marca_id')->unsigned();
            $table->integer('veiculo_id')->unsigned();
            $table->integer('ano_id')->unsigned();
            $table->timestamps();

            $table->foreign('tipo_id')->references('id')->on('veiculo_tipos');
            $table->foreign('marca_id')->references('id')->on('veiculo_marcas');
            $table->foreign('veiculo_id')->references('id')->on('veiculos');
            $table->foreign('ano_id')->references('id')->on('veiculo_anos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produto_veiculos');
    }
}
