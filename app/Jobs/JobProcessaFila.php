<?php

namespace App\Jobs;

use App\Helpers\Backend\Marktplace\HelperOpencart;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class JobProcessaFila implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $produtos = new HelperOpencart();
        $produtos->ProdutosFila('PRODUTO');
    }

    public function tags()
    {
        return ['Opencart', 'Aguardando processamento de ecommerce nas Empresas'];
    }
}
