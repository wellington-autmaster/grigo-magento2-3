<?php

namespace App\Jobs;

use App\Helpers\Backend\Marktplace\HelperMagento23;
use App\Models\Marktplace\Marktplace;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class FilaProdutosMagento23 implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $produto, $empresa;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Marktplace $empresa,$produto)
    {
        $this->produto = $produto;
        $this->empresa = $empresa;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::debug('na fila');
        $produto = new HelperMagento23();
        $produto->enviaProdutosMagento23($this->empresa,  $this->produto);


        /* if($this->produto <> '9999999'){
             $produto->ProdutosImagem($this->empresa,  $this->produto);
             $produto->enviaAtributos($this->empresa,  $this->produto);
             $produto->enviaSimilares($this->empresa,  $this->produto);

         }*/

    }

    public function tags()
    {
        return ['Magento-Produto', 'Emp:'.$this->empresa->empresa. ' - produto:'.$this->produto];
    }
}