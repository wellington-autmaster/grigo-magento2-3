<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;


class EventMonitorOrdemServico implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets ,SerializesModels;

    public $ordens;
    public $channel;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($ordens,$channel)
    {
        $this->ordens = $ordens;
        $this->channel = $channel;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastAs()
    {
        return 'ordem';
    }


    public function broadcastOn()
    {

        return new Channel('chordemservico');
        //return new PrivateChannel('chordemservico');
    }
}
