<?php

namespace App\Models\Empresa;

use App\Models\Marktplace\Marktplace;
use Illuminate\Database\Eloquent\Model;

class Configuracao extends Model
{
    protected $fillable = ['empresa','modulo','configuracao','valor','descricao'];

    public static function getConfigOc(Marktplace $empresa,  $configuracao )
    {
        $config = Configuracao::where('empresa', $empresa->empresa)->where('modulo','opencart')->where('configuracao',$configuracao)->first();
        return $config->valor;
    }

    public static function getConfigMagento23(Marktplace $empresa,  $configuracao )
    {
        $config = Configuracao::where('empresa', $empresa->empresa)->where('modulo','magento23')->where('configuracao',$configuracao)->first();
        return $config->valor;
    }


}
