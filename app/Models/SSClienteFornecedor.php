<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SSClienteFornecedor extends Model
{
    //Model do Clientes e Fornecedors
    protected $connection = 'pgsql';
    protected $table = 'pccdcli0';


    public static function getPessoa($codigo)
    {
        return SSClienteFornecedor::where('codigo',$codigo)
            ->select('codigo','descri')
            ->first();
    }
}
