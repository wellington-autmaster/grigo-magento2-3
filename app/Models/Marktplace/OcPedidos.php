<?php

namespace App\Models\Marktplace;

use App\Models\Empresa\Configuracao;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OcPedidos extends Model
{
    public static function getPedidos(Marktplace $empresa)
    {
        $api_token = Marktplace::OpencartToken($empresa->empresa);

        // Verifica qual status esta configurado para importar o pedido
        $status_ped_imp = Configuracao::getConfigOc($empresa,'status_pedido_importar');
        $headers = [
            'content-type' => 'application/json',
            'accept' => 'application/json',
            'Authorization' => 'Bearer ' . $api_token
        ];

        $url_api = parse_url($empresa->url, PHP_URL_SCHEME) . '://api.' . parse_url($empresa->url, PHP_URL_HOST) . '/';

        try {
            $client = new Client(['base_uri' => $url_api]);
            $request =  $client->request('GET', '/api/v1/pedidos/situacao/'.$status_ped_imp, [ 'headers'  => $headers]);
            $pedidos = $request->getBody();
            $pedidos = json_decode($pedidos);

            return $pedidos;

        } catch (ClientException $e) {
            Log::emergency($empresa->empresa. 'Oc Produto - Ocorreram problemas com a importação dos pedidos ' . $e->getMessage()  );
            return $e;
        };

    }

    public static function postOrderHistory(Marktplace $empresa, $pedido)
    {

        $api_token = Marktplace::OpencartToken($empresa->empresa);

        // Verifica qual status esta configurado apos importar o pedido no ssplus
        $status_ped_post = Configuracao::getConfigOc($empresa,'status_pos_importa_pedido');

        $arr_pedido = [
            'order_id' => $pedido,
            'history' => $status_ped_post,
            'comment' => ''
        ];
        $arr_pedido = json_encode($arr_pedido);
        $headers = [
            'content-type' => 'application/json',
            'accept' => 'application/json',
            'Authorization' => 'Bearer ' . $api_token
        ];
        $array = ['body' => $arr_pedido, 'headers' => $headers];

        $url_api = parse_url($empresa->url, PHP_URL_SCHEME) . '://api.' . parse_url($empresa->url, PHP_URL_HOST) . '/';

        try {
            $client = new Client(['base_uri' => $url_api]);
            $request =  $client->request('POST', '/api/v1/pedidos/add/historico', $array);
            $pedidos = $request->getBody();
            $pedidos = json_decode($pedidos);

            Log::info($empresa->empresa .'Oc - Atualizado histórico '.$status_ped_post.' do pedido '.$pedido);

        } catch (ClientException $e) {
            Log::emergency($empresa->empresa. 'Oc Produto - Ocorreram problemas com a atualização de histórico de pedidos
             ' . $e->getMessage()  );
            return $e;
        };
    }


}
