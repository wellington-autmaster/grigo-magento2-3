<?php

namespace App\Models\Marktplace;

use App\Jobs\jobAtualizaPedidoMagento;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;


class MagentoPedidos extends Model
{


    public static function getPedidosMagento(Marktplace $empresa, $status_pedido)
    {

        $url_api = parse_url($empresa->url, PHP_URL_SCHEME) . '://' . parse_url($empresa->url, PHP_URL_HOST);
        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => '*/*'
        ];
        $stack = HandlerStack::create();
        $oauth = new Oauth1(config('magento.Oauth1'));
        $stack->push($oauth);
        $client = new Client([
            'base_uri' => $url_api,
            'headers' => $headers,
            'handler' => $stack,
        ]);

        // Verifica qual status esta configurado para importar o pedido
        //  $status_ped_imp = Configuracao::getConfigMagento23($empresa,'status_pedido_importar');
        // $status = ($status_ped_imp == 1) ? $status= 'processing': (($status_ped_imp == 2) ? $status= 'pending_payment' : $status= false);

        //realiza busca dos pedidos
        $rota = '/rest/V1/orders/?searchCriteria[filterGroups][0][filters][0][field]=status&searchCriteria[filterGroups][0][filters][0][value]=' . $status_pedido;
        try {
            // Set the "auth" request option to "oauth" to sign using oauth
            $http = $client->get($rota, ['auth' => 'oauth']);

            if ($http->getStatusCode() != '200') {
                Log::error('Erro: ' . $http->getStatusCode() . 'Status: ' . $status_pedido .
                    ' Rota: ' . $rota);
                exit($http->getStatusCode() . 'Internal Server Error Magento');
            }

            $pedidos = json_decode($http->getBody());
            return $pedidos;

        } catch (ClientException $e) {
            Log::emergency($empresa->empresa . 'Produto Magento - Ocorreram problemas com a importação dos pedidos ' . $e->getMessage());
            return $e;
        };
    }

    public function atualizaPedidosFaturados()
    {
        $empresas = Marktplace::Empresas('magento23');

        foreach ($empresas as $empresa) {

            $status_atual_pedido = config('magento.status_apos_importar');
            $pedidos = $this->getPedidosMagento($empresa, $status_atual_pedido);
            $novo_status = config('magento.status_apos_faturar');

            foreach ($pedidos->items as $pedido)
            {
                $idpedidomagento = str_pad(($pedido->entity_id), 6, '0', STR_PAD_LEFT);

                $pedido_ssplus = DB::connection('pgsql')->table('ecommerce_pedido_status_faturamento_view')->where('empfil', $empresa->empresa)
                    ->where('pedido', $idpedidomagento)->first();

                if(!$pedido_ssplus) {
                    continue;
                }


                if($pedido_ssplus->status == "FATURADO") {
                    Log::notice('Pedido: '. $pedido->entity_id .' Faturado. NF-e: '. $pedido_ssplus->nfe_chave);
                    jobAtualizaPedidoMagento::dispatch($empresa, $pedido->entity_id, $novo_status)->onQueue('default');
                }

            }
        }


    }

    public static function atualizaStatusPedidoMagento($empresa, $entity_id, $novo_status)
    {
        $url_api = parse_url($empresa->url, PHP_URL_SCHEME) . '://' . parse_url($empresa->url, PHP_URL_HOST);


        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => '*/*'
        ];

        $arr_status = [
            'entity' => [
                'entity_id' => $entity_id,
                'state'     => $novo_status,
                'status'    => $novo_status
            ]
        ];

        $arr_status = json_encode($arr_status);

        $stack = HandlerStack::create();
        $oauth = new Oauth1(config('magento.Oauth1'));
        $stack->push($oauth);
        $client = new Client([
            'base_uri' => $url_api,
            'headers'  => $headers,
            'handler'  => $stack,
            'body' => $arr_status
        ]);


        try {
            // Set the "auth" request option to "oauth" to sign using oauth
            $http = $client->post("/rest/V1/orders/", ['auth' => 'oauth']);
            if($http->getStatusCode() != '200' ) { // Timeout interrompe o script
                Log::error($empresa->empresa. 'Não foi possível atualizar status do pedido no Magento.'. 'Pedido ID: '. $entity_id);
                exit($http->getStatusCode() . ' -  Erro interno ao atualizar status do pedido Magento');
            }

            //$res_status = $http->getBody()->getContents();

            return true;

        } catch (ClientException $e) {
            Log::error($empresa->empresa. 'Não foi possível atualizar status do pedido no Magento.' . $e->getMessage()  );
            return $e;
        };


    }




    public function getUrlApi(Marktplace $marktplace)
    {
        return parse_url($marktplace->url, PHP_URL_SCHEME). '://' . parse_url($marktplace->url, PHP_URL_HOST);
    }



}
