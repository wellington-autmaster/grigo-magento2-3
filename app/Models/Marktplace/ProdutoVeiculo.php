<?php

namespace App\Models\Marktplace;

use Illuminate\Database\Eloquent\Model;

class ProdutoVeiculo extends Model
{
    public function ano()
    {
        return $this->hasOne('App\Models\Marktplace\VeiculoAno', 'id', 'ano_id');

    }
}
