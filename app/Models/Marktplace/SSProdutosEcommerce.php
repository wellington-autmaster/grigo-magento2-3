<?php

namespace App\Models\Marktplace;

use App\Models\Empresa\Configuracao;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SSProdutosEcommerce extends Model
{
    protected $connection = 'pgsql';
    protected $table = 'produtos_ecommerce';

    /*public static function getProdutosComFoto($empresa)
    {
        //Seleciona todos os produtos com foto de acordo com a empresa
        $produtos =   SSProdutosEcommerce::where('foto_1', '<>', '')->where('codigo_empresa',$empresa)
            ->orderBy('data_atualizacao', 'desc')
            ->select('codigo_interno');

        //Verifica configuração de atualização geral ou diaria
        $configuracao = Configuracao::where('modulo', 'Marktplace')->where('empresa',$empresa)
            ->where('configuracao', 'atualizacao_geral_oc_img')->first();

        //Se atualizacao_geral for = F (FALSO) então atualiza somente produtos do dia
        if($configuracao->valor == 'F'){
            $produtos->where('data_atualizacao' , date('Y-m-d'));
        };

        //Volta configuração ao normal
        $configuracao->valor = 'F';
        $configuracao->save();

        return $produtos;
    }*/

    public static function ProdutosParaAtualizar($appCodigo, $filaAtualizacao)
    {
        /*Verifica as empresas com o app ativo*/
        $apps = Marktplace::Empresas($appCodigo);
        foreach ($apps as $app) {

            $SSProdutos =   SSProdutos::where(function ($query) {
                $query->where('envia_site', 't')
                      ->orWhere('excluir_site','t');
            })->where('ativo','1');

            //Verifica se a atualização é completa
            $atualizacao_geral = self::tipoDeAtualizacao($app->codigo, $app->empresa);
            ($atualizacao_geral == 'T') ?: $SSProdutos->where('data_atualizacao' , date('Y-m-d'));

            $SSProdutos->orderBy('codigo', 'asc')->select('codigo')->chunk(300, static function ($produtos) use ($app, $filaAtualizacao) {
                foreach ($produtos as $produto) {

                    $filaAtualizacao::dispatch($app, $produto->codigo)->onQueue('ecommerce');
                }
            });
        }
    }

    public static function tipoDeAtualizacao($integracao, $codigo_empresa)
    {
         //Verifica configuração de atualização geral ou diaria
        if($integracao === 'opencart') {
            $cfg_update = 'atualizacao_geral_oc';
        } elseif ($integracao === 'pluggto') {
            $cfg_update = 'atualizacao_geral_pt';
        }  elseif ($integracao === 'magento23') {
            $cfg_update = 'atualizacao_geral_oc';
        }


        $configuracao = Configuracao::where('modulo', 'Marktplace')->where('empresa', $codigo_empresa)->where('configuracao', $cfg_update)->first();
        $configuracao_atual = $configuracao->valor;

        // Volta configuração para atualização diaria;
        $configuracao->valor = 'F';
        $configuracao->save();

        return $configuracao_atual;
    }

    public static function getCaracteristicas()
    {
        //Retorna todos os produtos com caracteristicas
        return DB::connection('pgsql')->table('produtos_ecommerce_caracteristicas')->orderBy('codigo_interno', 'desc')
            ->select('codigo_interno')->distinct();
    }

    public static function getProdutos($empresa, $string)
    {
    }


}
