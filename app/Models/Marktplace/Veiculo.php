<?php

namespace App\Models\Marktplace;

use Illuminate\Database\Eloquent\Model;

class Veiculo extends Model
{
    public function GETmarca()
    {
        return $this->hasOne('App\Models\Marktplace\VeiculoMarca','id','veiculo_marca_id');
    }

    public function anos()
    {
        return $this->hasMany('App\Models\Marktplace\VeiculoAno');
    }
}
