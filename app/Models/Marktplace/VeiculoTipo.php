<?php

namespace App\Models\Marktplace;

use Illuminate\Database\Eloquent\Model;

class VeiculoTipo extends Model
{
    public function marcas()
    {
        return $this->hasMany('\App\Models\Marktplace\VeiculoMarca');
    }
}
