<?php

namespace App\Models\Marktplace;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class OcPedidosStatus extends Model
{
    public static function getPedidos(Marktplace $empresa)
    {
        $api_token = Marktplace::OpencartToken($empresa->empresa);

        $headers = [
            'content-type' => 'application/json',
            'accept' => 'application/json',
            'Authorization' => 'Bearer ' . $api_token
        ];

        $url_api = parse_url($empresa->url, PHP_URL_SCHEME) . '://api.' . parse_url($empresa->url, PHP_URL_HOST) . '/';

        try {
            $client = new Client(['base_uri' => $url_api]);
            $request =  $client->request('GET', '/api/v1/pedidos/status', [ 'headers' => $headers]);

            $status = $request->getBody();
            $status = json_decode($status);



            return $status;



        } catch (ClientException $e) {
            Log::emergency($empresa->empresa. 'Oc Produto - Ocorreram problemas com a importação dos pedidos ' . $e->getMessage()  );
            return $e;
        };



    }
}
