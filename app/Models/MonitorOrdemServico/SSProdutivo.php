<?php

namespace App\Models\MonitorOrdemServico;

use Illuminate\Database\Eloquent\Model;

class SSProdutivo extends Model
{
    //Model do Produtivo
    protected $connection = 'pgsql';
    protected $table = 'pcmelsm0';


    public static function getProdutivo($empresa, $os)
    {
      return  SSProdutivo::where('empfil', $empresa)->where('ordser',$os)->first();
    }
}
