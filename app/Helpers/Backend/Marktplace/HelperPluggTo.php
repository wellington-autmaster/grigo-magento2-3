<?php

namespace App\Helpers\Backend\Marktplace;

use App\Http\Controllers\Backend\Marktplace\Produto\ImgSSplusController;
use App\Jobs\JobFilaPluggto;
use App\Models\Marktplace\Marktplace;
use App\Models\Marktplace\ProdutoCaracteristica;
use App\Notifications\Backend\Marktplace\UpdateProdutos;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\Marktplace\SSProdutosEcommerce;
use GuzzleHttp\Client;
use App\Models\Auth\User;
use Illuminate\Support\Facades\Notification;


class HelperPluggTo
{

    /**
     * @param $i = Recebe o tipo de atualização (Diaria, completa ou por produto)
     * @param $produto Recebe o codigo do produto em caso de atualização diaria (Tem que combinar com a $i)
     * @return null
     */

   /* public function ProdutosFila()
    {
        //Verifica os marktplaces de cada empresa
        $empresas = Marktplace::Empresas('plugto');

        foreach ($empresas as $empresa) {
            Marktplace::plugtoToken($empresa->empresa);   //  Armazena o token no cache

            SSProdutosEcommerce::getProdutos($empresa->empresa, 'pluggto')->chunk(300, function ($produtos) use ($empresa) {
                foreach ($produtos as $produto) {
                    JobFilaPluggto::dispatch($empresa, $produto->codigo)->onQueue('pluggto');
                }
            });
            JobFilaPluggto::dispatch($empresa, '9999999')->onQueue('pluggto');

        }
        return true;
    }*/

    public function enviaProdutosPlugto($empresa, $produto)
    {

        if ($produto == '9999999') {
            $users = User::all();
            Notification::send($users, new UpdateProdutos('Todos os produtos da Plugg.to foram atualizados para empresa ' . $empresa->empresa));
            return true;
        }

        // $api_token = Cache::get('oc_key_' . $empresa->empresa);
        $api_token = Marktplace::plugtoToken($empresa->empresa);
        $produto = SSProdutosEcommerce::where('codigo_interno', $produto)->where('codigo_empresa', $empresa->empresa)->first();

        //Verifica se o produto esta ativo ou inativo para e-commerce, caso inativo
        if($produto->excluir_site){
            $this->inativaProdutos($produto,$api_token,$empresa);
            return response('Atualização Finalizada', 200);
        }

        // Ajusta as categorias Secao, grupo e subgrupo do produto
        $categorias = [
            ["name" => $produto->secao_descricao],
            ["name" => $produto->secao_descricao . ' > ' . $produto->grupo_descricao],
            ["name" => $produto->secao_descricao . ' > ' . $produto->grupo_descricao . ' > ' . $produto->subgrupo_descricao],
        ];

        $dimensao = [
            "length" => $produto->comprimento,
            "width" => $produto->largura,
            "height" => $produto->altura,
            "weight" => $produto->peso_bruto
        ];

        $atributos = [];

        $atributos = HelperPluggTo::enviaAtributos($produto->codigo_interno);
        // Ajusta codigo anp do cadastro do produto como um atrubuto no plugg.to pois não há campo prorpio
        if ($produto->anp > 0) {
            array_push($atributos, [
                "code" => 'ANP',
                "label" => 'ANP',
                "value" => [
                    "code" => $produto->anp,
                    "label" => $produto->anp
                ]
            ]);
        }

        array_push($atributos, [
            "code" => 'TAMANHO',
            "label" => 'TAMANHO',
            "value" => [
                "code" => 'UNICO',
                "label" => 'ÚNICO'
            ]
        ]);
        array_push($atributos, [
            "code" => 'COR',
            "label" => 'COR',
            "value" => [
                "code" => 'UNICO',
                "label" => 'ÚNICO'
            ]
        ]);

        if($produto->marca == '') {
            $produto->marca = 'GERAL';
        }
        array_push($atributos, [
            "code" => 'MARCA',
            "label" => 'MARCA',
            "value" => [
                "code" => str_replace(" ", "",$produto->marca),
                "label" => trim($produto->marca)
            ]
        ]);

        //  $caracateristicas = ProdutoCaracteristica::produtoCarateristica($produto->codigo_interno);

        /*if($caracateristicas){
            foreach($caracateristicas as $caracateristica){
                array_push($atributos,[
                  //  "code" => $caracateristica['id_descricao'],
                    "code" => str_replace(" ", "", $caracateristica['descricao']),
                    "label"=> $caracateristica['descricao'],
                    "value"=> [
                        "code"=> str_replace(" ", "",$caracateristica['valor']),
                        "label"=> $caracateristica['valor']
                        //"label"=> trim($caracateristica['valor_id'], " ")
                       // "label"=> $caracateristica['valor']
                    ]
                ]);
            };
        };*/






        $variacoes = [];

     /*   $fotos = [
            ["foto" => $produto->foto_1, "ordem" => 1],
            ["foto" => $produto->foto_2, "ordem" => 2],
            ["foto" => $produto->foto_3, "ordem" => 3],
            ["foto" => $produto->foto_4, "ordem" => 4],
        ];*/

        $imagens = [];


        /*foreach ($fotos as $foto) {
            if (isset($foto['foto'])) {
                $link = str_finish(config('app.url'), '/') . 'imagem/produto/' . $empresa->empresa . '/foto_' . $foto['ordem'] . '/' . $produto->codigo_interno;
                array_push($imagens, [
                    "url" => $link,
                    "name" => $produto->descricao,
                    "title" => $produto->descricao,
                    "order" => $foto['ordem'],
                    "external" => $link
                ]);

            };
        }*/
        $fotos =  ImgSSplusController::ProdutosImagem($produto->codigo_interno);

        $imagens = [];

        foreach ($fotos as $foto) {

            $link = str_finish(config('app.url'), '/') . $foto['url'];

            array_push($imagens, [
                "url" => $link,
                "name" => $produto->descricao,
                "title" => $produto->descricao,
                "order" => $foto['ordem'],
                "external" => $link
            ]);
        }

        //Ajusta quebra de linha da aplicação
        $produto->aplicacao = str_replace("\r", "<br>", $produto->aplicacao);

        $dados_produto = array(
            "sku" => $produto->codigo_interno,
            "ean" => $produto->codigo_barras,
            "ncm" => $produto->ncm,
            "name" => $this->limitarTexto($produto->descricao, 60),
            "external" => $produto->codigo_interno,
            "quantity" => $produto->estoque_disponivel,
            "special_price" => $produto->preco_venda,
            "price" => $produto->preco_venda,
            "short_description" => $produto->descricao,
            "description" => $produto->aplicacao,
            "brand" => $produto->marca,
            "cost" => $produto->preco_venda,
            "model" => $produto->codigo_fabricante,
            "warranty_time" => 3,
            "warranty_message" => "3 meses de garantia",
            "link" => "",
            "available" => 1,
            "categories" => $categorias,
            "handling_time" => 1,
            "manufacture_time" => 1,
            "dimension" => $dimensao,
            "attributes" => $atributos,
            "photos" => $imagens,
            "variations" => $variacoes,
        );

        $dados_produto = json_encode($dados_produto);

        $headers = [
            'content-type' => 'application/json',
            'accept' => 'application/json',
        ];

        $array = ['body' => $dados_produto, 'headers' => $headers];
        try {
            $client = new Client();
            $response = $client->request('PUT', 'https://api.plugg.to/skus/'
                . $produto->codigo_interno . '?access_token=' . $api_token, $array);
            Log::info($empresa->empresa . ' - Plugg.to - Produto ' . $produto->codigo_interno . ' atualizado.');

        } catch (\GuzzleHttp\Exception\ClientException $e) {
            if ($e->getResponse()->getStatusCode() == 400) {
                Log::info('Plugg.to  - Produto sem modificações -  ' . $produto->codigo_interno);
            } else {
                Log::error($e->getResponse()->getStatusCode() . ' - ' . $e->getResponse()->getReasonPhrase() . ' - Plugg.to - Produto não atualizou - ' . $produto->codigo_interno . "\n Informações do Prouto \n" . $dados_produto . "\n\n\n" . $e);
            }
        }

        return response('Atualização Finalizada', 200);

    } // function


    public static function enviaAtributos($produto)
    {

        Log::debug('Pesquisando atributos do produto '. $produto);
        $arr_atributos = [];

        $atributos = DB::connection('pgsql')->table('produtos_ecommerce_caracteristicas')
            ->where('codigo_interno', $produto)->get();

        if ($atributos->count() > 0) {
            Log::debug('Atributos localizados');

            foreach ($atributos as $atributo) {

                array_push($arr_atributos, [
                    "code" => str_replace(" ", "", $atributo->descricao_caracteristica),
                    "label" => trim($atributo->descricao_caracteristica),
                    "value" => [
                        "code" => str_replace(" ", "", $atributo->valor_caracteristica),
                        "label" => $atributo->valor_caracteristica
                    ]
                ]);
            }
        }

        return $arr_atributos;
    }

///O L D
///
///

    public  function enviaProdutosPlugtobkp($i, $produto)
    {

        //Seleciona todas as empresas com plugto ativo no cadastro de marktplace
        $empresas = Marktplace::where('status', true)->where('codigo', 'plugto')->get();

        $validade = '00:00:00';

        foreach ($empresas as $empresa) {
            //Verifica o tipo de atualização
            $up = Marktplace::TipoAtualizacao($i, $produto, $empresa->empresa, 'atualizacao_geral_pt');

            Log::info('Plugg.to - Iniciando processo de atualização PlugTo da empresa ' . $empresa->empresa . ' Tipo: ' . $up['valor']);
            $plugto = Marktplace::plugtoToken($empresa->empresa);


            SSProdutosEcommerce::where('codigo_empresa', $empresa->empresa)
                ->where($up['coluna'], $up['operador'], $up['valor'])
                ->orderBy('codigo_interno', 'desc')->chunk(100, function ($produtos) use ($empresa, $plugto) {

                    foreach ($produtos as $produto) {

                        if (strtotime($plugto->validade) <= strtotime(date('H:i:s'))) {
                            $plugto = Marktplace::plugtoRefreshToken($empresa->empresa, $plugto->refresh_token);
                            Log::info('Gerou Refrsh Token');
                        }


                        // Ajusta as categorias Secao, grupo e subgrupo do produto
                        $categorias = [
                            ["name" => $produto->secao_descricao],
                            ["name" => $produto->secao_descricao . ' > ' . $produto->grupo_descricao],
                            ["name" => $produto->secao_descricao . ' > ' . $produto->grupo_descricao . ' > ' . $produto->subgrupo_descricao],
                        ];

                        $dimensao = [
                            "length" => $produto->comprimento,
                            "width" => $produto->largura,
                            "height" => $produto->altura,
                            "weight" => $produto->peso_bruto
                        ];

                        $atributos = [];

                        $atributos = $this->enviaAtributos($empresa->empresa, $produto->codigo_interno);
                        // Ajusta codigo anp do cadastro do produto como um atrubuto no plugg.to pois não há campo prorpio
                        if ($produto->anp > 0) {
                            array_push($atributos, [
                                "code" => 'ANP',
                                "label" => 'ANP',
                                "value" => [
                                    "code" => $produto->anp,
                                    "label" => $produto->anp
                                ]
                            ]);
                        }

                        array_push($atributos, [
                            "code" => 'TAMANHO',
                            "label" => 'TAMANHO',
                            "value" => [
                                "code" => 'UNICO',
                                "label" => 'ÚNICO'
                            ]
                        ]);
                        array_push($atributos, [
                            "code" => 'COR',
                            "label" => 'COR',
                            "value" => [
                                "code" => 'UNICO',
                                "label" => 'ÚNICO'
                            ]
                        ]);

                        //  $caracateristicas = ProdutoCaracteristica::produtoCarateristica($produto->codigo_interno);

                        /*if($caracateristicas){
                            foreach($caracateristicas as $caracateristica){
                                array_push($atributos,[
                                  //  "code" => $caracateristica['id_descricao'],
                                    "code" => str_replace(" ", "", $caracateristica['descricao']),
                                    "label"=> $caracateristica['descricao'],
                                    "value"=> [
                                        "code"=> str_replace(" ", "",$caracateristica['valor']),
                                        "label"=> $caracateristica['valor']
                                        //"label"=> trim($caracateristica['valor_id'], " ")
                                       // "label"=> $caracateristica['valor']
                                    ]
                                ]);
                            };
                        };*/

                        $variacoes = [];

                        /*   $fotos = [
                               ["foto" => $produto->foto_1, "ordem" => 1],
                               ["foto" => $produto->foto_2, "ordem" => 2],
                               ["foto" => $produto->foto_3, "ordem" => 3],
                               ["foto" => $produto->foto_4, "ordem" => 4],
                           ];*/

                        $fotos =  ImgSSplusController::ProdutosImagem($produto->codigo_interno);

                        $imagens = [];

                        foreach ($fotos as $foto) {

                            $link = str_finish(config('app.url'), '/') . $foto->url;

                            array_push($imagens, [
                                "url" => $link,
                                "name" => $produto->descricao,
                                "title" => $produto->descricao,
                                "order" => $foto->ordem,
                                "external" => $link
                            ]);
                        }

                        //Ajusta quebra de linha da aplicação
                        $produto->aplicacao = str_replace("\r", "<br>", $produto->aplicacao);

                        $dados_produto = array(
                            "sku" => $produto->codigo_interno,
                            "ean" => $produto->codigo_barras,
                            "ncm" => $produto->ncm,
                            "name" => $this->limitarTexto($produto->descricao, 60),
                            "external" => $produto->codigo_interno,
                            "quantity" => $produto->estoque_disponivel,
                            "special_price" => $produto->preco_venda,
                            "price" => $produto->preco_venda,
                            "short_description" => $produto->descricao,
                            "description" => $produto->aplicacao,
                            "brand" => $produto->marca,
                            "cost" => $produto->preco_venda,
                            "model" => $produto->codigo_fabricante,
                            "warranty_time" => 3,
                            "warranty_message" => "3 meses de garantia",
                            "link" => "",
                            "available" => 1,
                            "categories" => $categorias,
                            "handling_time" => 1,
                            "manufacture_time" => 1,
                            "dimension" => $dimensao,
                            "attributes" => $atributos,
                            "photos" => $imagens,
                            "variations" => $variacoes,
                        );

                        $dados_produto = json_encode($dados_produto);

                        $headers = [
                            'content-type' => 'application/json',
                            'accept' => 'application/json',
                        ];

                        $array = ['body' => $dados_produto, 'headers' => $headers];
                        try {
                            $client = new Client();
                            $response = $client->request('PUT', 'https://api.plugg.to/skus/'
                                . $produto->codigo_interno . '?access_token=' . $plugto->access_token, $array);
                            Log::info($empresa->empresa . ' - Plugg.to - Produto ' . $produto->codigo_interno . ' atualizado.');

                        } catch (\GuzzleHttp\Exception\ClientException $e) {
                            if ($e->getResponse()->getStatusCode() == 400) {
                                Log::info('Plugg.to  - Produto sem modificações -  ' . $produto->codigo_interno);
                            } else {
                                Log::error($e->getResponse()->getStatusCode() . ' - ' . $e->getResponse()->getReasonPhrase() . ' - Plugg.to - Produto não atualizou - ' . $produto->codigo_interno . "\n Informações do Prouto \n" . $dados_produto . "\n\n\n" . $e);
                            }
                        }
                    } // foreach produtos

                });

            Log::info($empresa->empresa . ' - Plugg.to  - Atualização da empresa concluída.');

        }// Foreach empresa por empresa


        Log::info('Plugg.to  - Todas as atualizações foram concluídas.');
        return response('Atualização Finalizada', 200);

    } // function

    public function limitarTexto($texto, $limite){
        $contador = strlen($texto);
        if ( $contador >= $limite ) {
            $texto = substr($texto, 0, strrpos(substr($texto, 0, $limite), ' ')) ;
            return $texto;
        }
        else{
            return $texto;
        }
    }


    public function inativaProdutos($produto, $token, $empresa)
    {


        //https://api.plugg.to/skus/{sku_id}?access_token={access_token}

        $headers = [
            'content-type' => 'application/json',
            'accept' => 'application/json',
        ];

        $array = ['headers' => $headers];
        try {
            $client = new Client();
            $response = $client->request('DELETE', 'https://api.plugg.to/skus/'.$produto->codigo_interno.'?access_token='.$token, $array);
            Log::info($empresa->empresa . ' - Plugg.to - Produto ' . $produto->codigo_interno . ' removido.');

        } catch (\GuzzleHttp\Exception\ClientException $e) {
            if ($e->getResponse()->getStatusCode() == 400) {
                Log::info('Plugg.to  - Produto sem modificações -  ' . $produto->codigo_interno);
            } else {
                Log::error($e->getResponse()->getStatusCode() . ' - ' . $e->getResponse()->getReasonPhrase() . ' - Plugg.to - Produto não atualizou - ' . $produto->codigo_interno . "\n Informações do Prouto \n"  . $e);
            }
        }
    }




} //class
