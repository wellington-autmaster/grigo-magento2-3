<?php
/**
 * Created by PhpStorm.
 * User: wanderlei
 * Date: 20/09/18
 * Time: 10:32
 */

namespace App\Helpers\Backend\Marktplace;

use App\Helpers\Auth\Auth;
use App\Jobs\FilaProdutosOc;
use App\MasterProcess;
use App\Models\Auth\User;
use App\Models\Empresa\Configuracao;
use App\Models\Empresa\Empresa;
use App\Models\Marktplace\Marktplace;
use App\Models\Marktplace\SSProdutosEcommerce;
use App\Modulo;
use App\Notifications\Backend\Marktplace\UpdateProdutos;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class HelperOpencart
{
    public function ProdutosFila()
    {
        Log::info('Iniciando atualização de produtos do Opencart');

        //Verifica os marktplaces de cada empresa
        $empresas = Marktplace::Empresas('opencart');

        foreach ($empresas as $empresa) {
             Marktplace::OpencartToken($empresa->empresa);   //  Armazena o token no cache

            SSProdutosEcommerce::getProdutos($empresa->empresa, 'opencart')->chunk(300, function ($produtos) use ($empresa) {
                foreach ($produtos as $produto) {
                    FilaProdutosOc::dispatch($empresa, $produto->codigo)->onQueue('ecommerce');
                }
            });
            FilaProdutosOc::dispatch($empresa, '9999999')->onQueue('ecommerce');
        }
        return true;
    }

    public function enviaProdutosOpencart($empresa, $produto)
    {


        $api_token = Marktplace::OpencartToken($empresa->empresa);
        $produto = SSProdutosEcommerce::where('codigo_interno', $produto)->where('codigo_empresa',$empresa->empresa)->first();

        $arrprodutos = [];

        // Se o produto tiver erros que não podem ser exibidos, o produto não é enviado.
        if (!$this->validaProduto($produto)) {
            return false;
        }

        // Valida configurações de atualização
        $produto = $this->validaConfiguracoes($empresa->empresa, $produto);

        //verifica se o modulo esta ativo
        $moduloOpencart = MasterProcess::statusContrato($empresa, 'Opencart');
        $moduloOpencart->producao ? $nomeProduto = $produto->descricao : $nomeProduto =  ' [HOMOLOGAÇÃO] '.$produto->descricao ;

        $arrprodutos[] = [
            'product_id' => $produto->codigo_interno,
            'model' => $produto->codigo_fabricante,
            'sku' => $produto->codigo_interno,
            'ean' => $produto->codigo_barras,
            'status' => $produto->status,
            'quantity' => $produto->estoque_disponivel,
            'image' => '',
            'price' => $produto->preco_venda,
            'weight' => $produto->peso_bruto,
            'length' => $produto->comprimento,
            'width' => $produto->largura,
            'height' => $produto->altura,
            'name' => $nomeProduto,
            'description' => str_replace("\r", "<br>", $produto->aplicacao),
            'price_special' => $produto->preco_promocao,
            'date_start' => $produto->data_inicio_promocao,
            'date_end' => $produto->data_fim_promocao,
            'codigo_secao_ext' => $produto->secao,
            'secao_name' => $produto->secao_descricao,
            'codigo_grupo_ext' => $produto->grupo,
            'grupo_name' => $produto->grupo_descricao,
            'secao_top' => 1,
            'manufacturer_name' => $produto->marca
        ];

        $arrprodutos = json_encode($arrprodutos);

        $headers = [
            'content-type' => 'application/json',
            'accept' => 'application/json',
            'Authorization' => 'Bearer ' . $api_token
        ];

        $array = ['body' => $arrprodutos, 'headers' => $headers];
        $url_api = $this->getUrlApi($empresa);

        try {
            // Create a client with a base URI
            $client = new Client(['base_uri' => $url_api]);
            $response = $client->request('POST', 'api/v1/products', $array);
            Log::info($empresa->empresa. ' Oc Produto: ' . $response->getBody());

        } catch (ClientException $e) {
            Log::error($empresa->empresa. 'Oc Produto - Ocorreram problemas com atualização do produto '. $produto->codigo_interno.' - ' . $e->getMessage()  ,[$produto]);
        };

        /** Chunk **/
        return true;
    }

    public function validaProduto($produto)
    {
        if (trim($produto->codigo_fabricante) == null) {
            Log::alert('Produto não possui Código do Fabricante no cadastro do SSPlus',
                ['cod: ' => $produto->codigo_interno]);

            return false;
        };

        if (trim($produto->codigo_barras) == null) {
            Log::alert('Produto não possui código de barras no cadastro do SSPlus',
                ['cod: ' => $produto->codigo_interno]);
        }

        if (trim($produto->preco_venda) == '0.00') {
            Log::emergency('Não é possivel enviar um produto sem preço de venda definido no cadastro',
                ['cod: ' => $produto->codigo_interno]);

            return false;
        }

        if (trim($produto->peso_bruto) == null) {
            Log::alert('O peso do produto não esta definido no cadastro, valor 1 é definido como padrão.',
                ['cod: ' => $produto->codigo_interno]);

            $produto->peso_bruto = 1;
        }

        if (trim($produto->comprimento) == null) {
            Log::alert('O comprimento do produto não esta definido no cadastro, valor 1 é definido como padrão',
                ['cod: ' => $produto->codigo_interno]);
            $produto->comprimento = 1;
        }

        if (trim($produto->largura) == null) {
            Log::alert('A largura do produto não esta definido no cadastro, valor 1 é definido como padrão',
                ['cod: ' => $produto->codigo_interno]);
            $produto->largura = 1;
        }

        if (trim($produto->altura) == null) {
            Log::alert('A altura do produto não esta definido no cadastro,  valor 1 é definido como padrão',
                ['cod: ' => $produto->codigo_interno]);

            $produto->altura = 1;
        }
        if (trim($produto->descricao) == null) {
            Log::emergency('Produto não possui descrição no cadastro',
                ['cod: ' => $produto->codigo_interno]);
            return false;
        }

        if (trim($produto->aplicacao) == null) {
            Log::alert('Produto não possui aplicação no cadastro ',
                ['cod: ' => $produto->codigo_interno]);
            $produto->aplicacao = $produto->descricao;
        }

        if (trim($produto->secao) == null) {
            Log::emergency('Produto  não possui secao',
                ['cod: ' => $produto->codigo_interno]);
            return false;
        }

        if (trim($produto->grupo) == null) {
            Log::emergency('Produto  não possui grupo',
                ['cod: ' => $produto->codigo_interno]);
            return false;
        }

        if (trim($produto->grupo_descricao) == null) {
            Log::emergency('Produto não possui descricao no grupo',
                ['cod: ' => $produto->codigo_interno]);
            return false;
        }

        if (trim($produto->secao_descricao) == null) {
            Log::emergency('Produto  não possui descricao na secao',
                ['cod: ' => $produto->codigo_interno]);
            return false;
        }

        if (trim($produto->marca) == null) {
            Log::alert('Produto não possui Marca definida, por padrão será relacionado a marca "Geral"',
                ['Cod:' => $produto->codigo_interno]);
            $produto->marca = "GERAL";
        }

        //Valida se esta ativo ou não
        $produto->status = $produto->excluir_site == false ? true : false;

        //Se estiver marcado para verificar foto, verifica se existe fotos no produto, se não existir o produto é inativado no e-commerce
        if(env('APP_ECOMM_VERIFICA_FOTO')){
            if(!$produto->foto_1){
                $produto->status = false;
            }
        }


        // Enriquece aplicação
        $aplica =  "<strong>Descrição: </strong> $produto->descricao <br>" .
                               "<strong>Marca: </strong> $produto->marca <br>" .
                               "<strong>Código interno: </strong>  $produto->codigo_interno<br>" .
                               "<strong>Código Fabricante: </strong>  $produto->codigo_fabricante<br>" .
                               "<strong>Código de barras: </strong>  $produto->codigo_barras<br><hr><br> $produto->aplicacao  "   ;
       $produto->aplicacao = $aplica;


        /** No aba e-commerce do SSPlus é possivel personalizar uma seção, caso ela esteja preenchida no cadastro de
         * produtos, irá substituir a da aba principal.
         * secao = recebe codigo 8 na frente
         * codigo_secao_ecommerce = Recebe codigo 9 na frente
         */
        if (trim($produto->descricao_secao_ecommerce) != trim($produto->secao_descricao) &&
            trim($produto->codigo_secao_ecommerce) != null &&
            trim($produto->descricao_secao_ecommerce) != null
            ) { //não deixa gravar outra seção se a descrição for igua a primcipal
            $produto->secao_descricao = trim($produto->descricao_secao_ecommerce);
            $produto->secao = (integer)'9' . $produto->codigo_secao_ecommerce;

        } else {
            $produto->secao = (integer)'8' . $produto->secao;
        }

        $produto->product_id = (integer)$produto->product_id;

        return $produto;
    }

    public function validaConfiguracoes($empresa, $produto)
    {
        //Configurações -- Valida promoção
        $config = Configuracao::where('modulo','opencart')->where('configuracao','enviar_promocao')->where('empresa',$empresa)->first();
        if($config->valor == false){
            $produto->preco_promocao = '';
            $produto->data_inicio_promocao = '';
            $produto->data_fim_promocao = '';
        };

        //marca_na_descricao
        $config = Configuracao::where('modulo','opencart')->where('configuracao','marca_na_descricao')->where('empresa',$empresa)->first();
        if($config->valor == true){
            $produto->descricao = $produto->descricao.' '. $produto->marca;
        }

        //atualizar_descricao
        $config = Configuracao::where('modulo','opencart')->where('configuracao','atualizar_descricao')->where('empresa',$empresa)->first();
        $produto->description_update = $config->valor;

        //inativa_produto_sem_estoque
        $config = Configuracao::where('modulo','opencart')->where('configuracao','inativa_produto_sem_estoque')->where('empresa',$empresa)->first();
        if($config->valor == true){
            if($produto->estoque_disponivel <= 0){
                $produto->status = 0;
            }
        }
/*        Inativar produtos de classes não permitidas*/

        //Verificar classes configuradas para não enviar ao e-commerce
        $classe_nao_enviar = Configuracao::where('modulo','opencart')->where('configuracao','classe_nao_enviar')->where('empresa',$empresa)->first();
        $classesConfiguradas = explode(';', $classe_nao_enviar->valor); //O formato salvo ex: A,B,C...

        // Verifica se o produto esta atribuido a uma classe
        if( $produto->classe_abc == '' ) {
            $produto->classe_abc = [];
        }
        //Verifica se a classe do produto esta contido nas classes para não enviar ao e-commerce
       $classeAnalisada = array_diff([$produto->classe_abc], $classesConfiguradas);

        if($classeAnalisada !== [$produto->classe_abc]) {
            //Caso a classse esteja incluida na lista de bloqueio, desabilita o produto no e-commerce
            $produto->status = 0;
       }

        return $produto;

    }

    public function enviaSimilares($empresa, $produto)
    {
            // Seleciona os prdutos que tem MESTRE
            $produto = SSProdutosEcommerce::where('codigo_empresa', $empresa->empresa)->where('codigo_interno', $produto)->first();

            if($produto->produto_mestre != null)
            {
                $cod_produto = $produto->produto_mestre;
            }else{
               return true;
            }

            $arr_similares = [];

                // Pesquisa produtos com o MESTRE vinculado
                $similares = SSProdutosEcommerce::where('produto_mestre', $cod_produto)
                    ->where('produto_mestre', '<>', '')->where('codigo_empresa', $empresa->empresa)
                    ->select('codigo_interno')->get();

                $count = $similares->count();

                if($count >= 0){
                    foreach ($similares as $similar) {
                        $arr_similares[] = ['related_id' => (integer)$similar->codigo_interno];
                    }

                    if (trim($produto->produto_mestre) != null) {
                        $arr_similares[] = ['related_id' => (integer)$produto->produto_mestre];
                    };

                    $arr_produtos  = ['product_id' => (integer)$produto->codigo_interno, 'similares' => $arr_similares];

                    $arr_produtos = json_encode($arr_produtos);

                    $api_token = Marktplace::OpencartToken($empresa->empresa);

                    $headers = [
                        'content-type' => 'application/json',
                        'accept' => 'application/json',
                        'Authorization' => 'Bearer ' . $api_token
                    ];



                    $array = ['body' => $arr_produtos, 'headers' => $headers];
                    $url_api = $this->getUrlApi($empresa);
                    try {
                        // Create a client with a base URI
                        $client = new Client(['base_uri' => $url_api]);
                        $client->request('POST', 'api/v1/products/related', $array);

                    } catch (ClientException $e) {
                        Log::error($empresa->empresa .' Oc Similar - Ocorreram problemas com atualização dos similares' . $e->getMessage());
                    };

                }else{
                    return false;
                }

        return response('Opencart - Atualização de similares conluido na empresa ' . $empresa->empresa, 200);
    }



    /*public function ProdutosImagem($empresa, $produto)
    {
        $api_token = Marktplace::OpencartToken($empresa->empresa);   //   return $opencart['access_token'];

        $arr_produtos = [];

        $imagens = DB::connection('pgsql')->table('imagem_produto')->where('codigo', $produto)->get();

        $ultima_img = DB::connection('pgsql')->table('imagem_produto')->select('ordem')
            ->where('codigo', $produto)
            ->orderBy('ordem', 'desc')
            ->first();
        $arr_imagens = [];

        foreach ($imagens as $imagem) {
            //Ajusta codificação da imagem
            $my_bytea = stream_get_contents($imagem->arquivo);
            $my_string = pg_unescape_bytea($my_bytea);
            $html_data = htmlspecialchars($my_string);

            $arr_imagens[] = [
                'product_id' => $produto,
                'ordem' => $imagem->ordem,
                'imagem' => $html_data,
                'ultima_imagem' => $ultima_img->ordem
            ];
        }
        $arr_produtos[] = ['product_id' => $produto, 'imagens' => $arr_imagens];

        $arr_produtos = json_encode($arr_produtos);

        $headers = [
            'content-type' => 'application/json',
            'accept' => 'application/json',
            'Authorization' => 'Bearer ' . $api_token
        ];

        $array = ['body' => $arr_produtos, 'headers' => $headers];
        $url_api = $this->getUrlApi($empresa);
        // Create a client with a base URI
        $client = new Client(['base_uri' => $url_api]);
        $client->request('POST', 'api/v1/products/image', $array);


        return response('Atualização de imagens concluida com sucesso', 200);
    }*/

    /*public function enviaAtributos($empresa , $produto)
    {

            $arr_produto = [];
            $arr_atributos = [];

            $atributos = DB::connection('pgsql')->table('produtos_ecommerce_caracteristicas')
                ->where('codigo_interno', $produto)->get();

                if ($atributos->count() > 0) {

                    foreach ($atributos as $atributo) {
                        $arr_atributos[] = [
                            'attribute_id' => trim($atributo->codigo_caracteristica),
                            'attribute_name' => trim($atributo->descricao_caracteristica),
                            'attribute_valor' => trim($atributo->valor_caracteristica),
                            'filtro' => trim($atributo->filtro_ecommerce) == 'S' ? true : false
                        ];
                    }

                    $arr_produto[] = [
                        'product_id' => $produto,
                        'attributes' => $arr_atributos
                    ];
                }

            // return $arr_produtos;

            $api_token = Marktplace::OpencartToken($empresa->empresa);
            $headers = [
                'content-type' => 'application/json',
                'accept' => 'application/json',
                'Authorization' => 'Bearer ' . $api_token
            ];
        Log::debug('message', $arr_produto);

        $arr_produto = json_encode($arr_produto);

            $array = ['body' => $arr_produto, 'headers' => $headers];
            $url_api = $this->getUrlApi($empresa);

            try {
                // Create a client with a base URI
                $client = new Client(['base_uri' => $url_api]);
                $client->request('POST', 'api/v1/products/attributes', $array);

            } catch (ClientException $e) {
                Log::error($empresa->empresa . 'Oc Atributos: ' .$e->getMessage() );
            }

        return response('Atributos e filtros cadastrados', 200);

    }*/

    public function getUrlApi(Marktplace $marktplace)
    {
        return parse_url($marktplace->url, PHP_URL_SCHEME) . '://api.' . parse_url($marktplace->url, PHP_URL_HOST) . '/';

    }

    public function configuraCNPJ($empresa)
    {
        //Verifica se a loja esta em manutenção
        $status_opencaert = Modulo::where('empresa', $empresa->empresa)->where('descricao','opencart')->first();

        //Envia dados do cliente para API
        $api_token = Marktplace::OpencartToken($empresa->empresa);

        $headers = [
            'content-type' => 'application/json',
            'accept' => 'application/json',
            'Authorization' => 'Bearer ' . $api_token
        ];

        $conf = Empresa::where('codigo', $empresa->empresa)->first();
        $url_api = $this->getUrlApi($empresa);
        $arr_cnpj = [
            'cnpj' => $conf->cgc,
            'opencart' => $status_opencaert
        ];
        $arr_cnpj = json_encode($arr_cnpj);

        $array = ['json' => $arr_cnpj, 'headers' => $headers];
        $url_api = $this->getUrlApi($empresa);

        // Create a client with a base URI
        $client = new Client(['base_uri' => $url_api]);
        $response = $client->request('POST', 'api/v1/configura/cnpj', $array);

    }


}
