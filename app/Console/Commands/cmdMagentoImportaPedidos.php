<?php

namespace App\Console\Commands;

use App\Http\Controllers\Backend\Marktplace\MagentoPedidosController;
use Illuminate\Console\Command;


class cmdMagentoImportaPedidos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'magento:importa-pedidos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importa pedidos do e-commerce para o SSPlus de acordo com o status configurado';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        MagentoPedidosController::getPedidos();
    }
}
