<?php

namespace App\Console\Commands;


use App\Http\Controllers\Frontend\MonitorSeparacao\FrtMonitorSeparacaoController;
use Illuminate\Console\Command;


class cmdPromocoes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'separacao:promocoes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '[Desabilitado]  - Atualiza as prmoções no monitor de separação de mercadorias';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Desabilitado, irá fazer a comunicação direta, sem o pusher
        $promocoes = new FrtMonitorSeparacaoController();
        $promocoes = $promocoes::promocoes();
        $this->info($promocoes);

    }
}
