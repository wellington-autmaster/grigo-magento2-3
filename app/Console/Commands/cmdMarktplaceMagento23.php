<?php

namespace App\Console\Commands;


use App\Jobs\FilaProdutosMagento23;
use App\Models\Marktplace\SSProdutosEcommerce;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;


class cmdMarktplaceMagento23 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'marktplace:magento';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envia produtos para API de e-commerce (Imagens, filtros e atrubutos não são enviados por este comando)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $FilaProdutos =  FilaProdutosMagento23::class;
        SSProdutosEcommerce::ProdutosParaAtualizar('magento23',$FilaProdutos);
    }
}
