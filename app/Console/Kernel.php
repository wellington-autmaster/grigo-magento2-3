<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

/**
 * Class Kernel.
 */
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */

    protected $commands = [
        Commands\cmdAtualizaPedidosFaturados::class,
        Commands\cmdMarktplacePecaqui::class,
        Commands\cmdMonitorSeparacaoSenha::class,
        Commands\cmdOcImportaPedidos::class,
        Commands\cmdPromocoes::class,
        Commands\cmdMarktplacePluggTo::class,
        Commands\Opencart::class,
        Commands\cmdMagentoImportaPedidos::class,
        Commands\cmdMarktplaceMagento23::class,
        Commands\cmdAtualizaPrecoEstoque::class
    ];


    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('magento:importa-pedidos')->everyFiveMinutes();
        $schedule->command('magento:status_faturado')->everyFiveMinutes();
        $schedule->command('magento:preco')->hourly()->between('8:00', '21:00');
        $schedule->command('marktplace:magento')->dailyAt('22:00');

       //Horizon Metricas
        $schedule->command('horizon:snapshot')->everyFiveMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');


        require base_path('routes/console.php');
    }
}
