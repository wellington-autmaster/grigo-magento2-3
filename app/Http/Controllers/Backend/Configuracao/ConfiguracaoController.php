<?php

namespace App\Http\Controllers\Backend\Configuracao;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Empresa\Configuracao;
use App\Helpers\Auth\Auth;

class ConfiguracaoController extends Controller
{
    public function GetConfiguracao($modulo, $configuracao)
    {
        $configuracao = Configuracao::where('modulo', $modulo)
            ->where('configuracao',$configuracao)
            ->where('empresa',Auth()->user()->empresa_codigo )
            ->first();
        return $configuracao;
    }

    public function PostConfiguracao(Request $request)
    {
        //Atualiza informação de configurações
        $configuracao = Configuracao::where('modulo',$request->modulo)
            ->where('configuracao',$request->configuracao)
            ->where('empresa',Auth()->user()->empresa_codigo )
            ->first();

        $configuracao->valor = $request->valor;
        $configuracao->save();
        return response('Atualizado com sucesso!',200);
    }
}
