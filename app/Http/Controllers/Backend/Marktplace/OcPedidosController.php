<?php

namespace App\Http\Controllers\Backend\Marktplace;

use App\Jobs\ImportaPedidosOc;
use App\Models\Empresa\Configuracao;
use App\Models\Marktplace\Marktplace;
use App\Models\Marktplace\OcPedidos;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OcPedidosController extends Controller
{
    public static function getPedidos()
    {
        Log::info('Importação de pedidos iniciado');
        /** @var  $empresas - Todos os marktplaces ativos do Opencart
         Verifica se existem pedidos com o status configurado a importar*/
        $empresas = Marktplace::Empresas('opencart');

        foreach ($empresas as $empresa){
            $pedidos = OcPedidos::getPedidos($empresa);

            foreach ($pedidos as $pedido){

                $pedido->cliente->limite_credito =  (string) round(Configuracao::getConfigOc($empresa, 'limite_credito_cliente'));

                $pedido->cliente->fone2 ='';
                $atualiza_cliente = Configuracao::getConfigOc($empresa, 'atualiza_cliente_ss');
                $pedido->cliente->atualiza_cliente = $atualiza_cliente == true ? 'S':'N';

                ImportaPedidosOc::dispatch($empresa, $pedido)->onQueue('ecommerce');
            }
        }
        return 'Pedidos enviados para fila de processamento';
    }

    public function importaPedidosSSPlus(Marktplace $empresa, $pedido)
    {
        //Gera um token de transmissão
        $token = DB::connection('pgsql')->select("select f_ecommerce_token()");
        $pedido->cliente->token = $token[0]->f_ecommerce_token;

        // Configura dados do cliente e envia ao post.
        $cliente = $this->postCliente($empresa,$pedido->cliente);
        DB::connection('pgsql')->beginTransaction();
        $cadcliente = DB::connection('pgsql')->select("select f_ecommerce_cliente($cliente)");
        $status = $this->statusTransacao($cadcliente[0]->f_ecommerce_cliente);

        if ($status){
            DB::connection('pgsql')->commit();
        }else{
            DB::connection('pgsql')->rollBack();
            Log::error($empresa->empresa. ' Oc Pedido '.$pedido->produtos[0]->order_id .' '.  $cadcliente[0]->f_ecommerce_cliente );
            return false;
        }

        $forma_pag_ss = Configuracao::getConfigOc($empresa, 'forma_pagamento_ss');

        // Configura os produtos do pedido
        DB::connection('pgsql')->beginTransaction();
        foreach ($pedido->produtos as $produto){
            //Prepara informações do pedido no produto
            $observacao =  $pedido->obs  .' Frete:' .$pedido->frete_descricao. ' Valor: R$'. $pedido->frete_valor;
            $observacao = substr($observacao,0,20).'..';
            $product_id = str_pad($produto->product_id, 6, '0', STR_PAD_LEFT);

            $arr_produto="'{$pedido->cliente->token}',
                                    '{$empresa->empresa}',     
                                    '{$produto->order_id}', 
                                    '{$pedido->cliente->cpf_cnpj}',   
                                    '{$product_id}',      
                                     $produto->quantity, 
                                     $produto->price, 
                                    '{$observacao}',   
                                    '{$pedido->pagamento}',   
                                    '{$forma_pag_ss}',        
                                    '{$pedido->data->date}'   
            ";

            $cadproduto = DB::connection('pgsql')->select(" select f_ecommerce_insere_pedido($arr_produto)  ");
            $status = $this->statusTransacao($cadproduto[0]->f_ecommerce_insere_pedido);

            if ($status == false){
                DB::connection('pgsql')->rollBack();
                Log::error($empresa->empresa . ' Oc Pedido ' . $pedido->produtos[0]->order_id . ' ' . $cadproduto[0]->f_ecommerce_insere_pedido . ' não foi importado'. ' 
                ',(array)  $produto);
                break;
            };
        }

        if($status == true){
            DB::connection('pgsql')->commit();
            OcPedidos::postOrderHistory($empresa, $pedido->produtos[0]->order_id);
            Log::info($empresa->empresa. ' Oc Pedido '.$pedido->produtos[0]->order_id . ' importado com sucesso' );
        }else {
            return false;
        }

    }


    public function postCliente(Marktplace $empresa,$cliente)
    {
        $cliente ="'{$cliente->token}',
                    '{$cliente->nome}',     
                    '{$cliente->cpf_cnpj}', 
                    '{$cliente->email}',   
                    '{$cliente->fone}',    
                    '{$cliente->situacao}', 
                    '{$cliente->endececo}', 
                    '{$cliente->numero}',   
                    '{$cliente->cidade}',   
                    '{$cliente->cep}',      
                    '{$cliente->pais}',     
                    '{$this->getEstado(mb_strtoupper($cliente->estado))}',   
                    '{$cliente->ie}',       
                    '{$cliente->rg}',       
                    '{$cliente->limite_credito}',    
                    '{$cliente->fone2}',     
                    '{$cliente->bairro}',   
                    '{$cliente->atualiza_cliente}'";

        return $cliente;

    }

    public function statusTransacao($trans)
    {
        $codigo = explode(' ', $trans);

        switch ($codigo[0]) {
            case 'SSECOM+0004':
                $status = true;
                break;
            case 'SSECOM+0001':
                $status = true;
                break;
            case 'SSECOM+0002':
                $status = true;
                break;
            case 'SSECOM-9999':
                $status = false;
                break;
            case 'SSECOM-1048':
                $status = false;
                break;
            case 'SSECOM-7648':
                $status = false;
                break;
            case 'SSECOM-1448':
                $status = false;
                break;
            case 'SSECOM-1449':
                $status = false;
                break;
            case 'SSECOM-1347':
                $status = false;
                break;
            case 'SSECOM-1932':
                $status = false;
                break;
            case 'SSECOM-2330':
                $status = false;
                break;
            case 'SSECOM-2731':
                $status = false;
                break;
            case 'SSECOM-3147':
                $status = false;
                break;
            case 'SSECOM-1022':
                $status = false;
                break;
        }

        return $status;
    }

    public function processaFilaPedidos()
    {
        Log::info('Solicitação manual de importação de pedidos');

        shell_exec('php ../artisan opencart:importa-pedidos > /dev/null &');
        return back()->with('status','Os pedidos estão sendo enviados para fila de processamento.');
    }

    public  function getEstado($estado)
    {
        switch ($estado) {
            /* UFs */
            case "AC" :	$estado = "Acre";					break;
            case "AL" :	$estado = "Alagoas";				break;
            case "AM" :	$estado = "Amazonas";				break;
            case "AP" :	$estado = "Amapá";					break;
            case "BA" :	$estado = "Bahia";					break;
            case "CE" :	$estado = "Ceará";					break;
            case "DF" :	$estado = "Distrito Federal";		break;
            case "ES" :	$estado = "Espírito Santo";		    break;
            case "GO" :	$estado = "Goiás";					break;
            case "MA" :	$estado = "Maranhão";				break;
            case "MG" :	$estado = "Minas Gerais";			break;
            case "MS" :	$estado = "Mato Grosso do Sul"; 	break;
            case "MT" :	$estado = "Mato Grosso";			break;
            case "PA" :	$estado = "Pará";					break;
            case "PB" :	$estado = "Paraíba";				break;
            case "PE" :	$estado = "Pernambuco";			    break;
            case "PI" :	$estado = "Piauí";					break;
            case "PR" :	$estado = "Paraná";				    break;
            case "RJ" :	$estado = "Rio de Janeiro";		    break;
            case "RN" :	$estado = "Rio Grande do Norte";	break;
            case "RO" :	$estado = "Rondônia";				break;
            case "RR" :	$estado = "Roraima";				break;
            case "RS" :	$estado = "Rio Grande do Sul";		break;
            case "SC" :	$estado = "Santa Catarina";		    break;
            case "SE" :	$estado = "Sergipe";				break;
            case "SP" :	$estado = "São Paulo";				break;
            case "TO" :	$estado = "Tocantíns";				break;

            /* Estados */
            case "ACRE" :					$estado = "AC";	break;
            case "ALAGOAS" :				$estado = "AL";	break;
            case "AMAZONAS" :				$estado = "AM";	break;
            case "AMAPÁ" :					$estado = "AP";	break;
            case "BAHIA" :					$estado = "BA";	break;
            case "CEARÁ" :					$estado = "CE";	break;
            case "DISTRITO FEDERAL" :		$estado = "DF";	break;
            case "ESPÍRITO SANTO" :			$estado = "ES";	break;
            case "GOIÁS" :					$estado = "GO";	break;
            case "MARANHÃO" :				$estado = "MA";	break;
            case "MINAS GERAIS" :			$estado = "MG";	break;
            case "MATO GROSSO DO SUL" :		$estado = "MS";	break;
            case "MATO GROSSO" :			$estado = "MT";	break;
            case "PARÁ" :					$estado = "PA";	break;
            case "PARAÍBA" :				$estado = "PB";	break;
            case "PERNAMBUCO" :				$estado = "PE";	break;
            case "PIAUÍ" :					$estado = "PI";	break;
            case "PARANÁ" :					$estado = "PR";	break;
            case "RIO DE JANEIRO" :			$estado = "RJ";	break;
            case "RIO GRANDE DO NORTE" :	$estado = "RN";	break;
            case "RONDÔNIA" : 				$estado = "RO";	break;
            case "RORAIMA" :				$estado = "RR";	break;
            case "RIO GRANDE DO SUL" :		$estado = "RS";	break;
            case "SANTA CATARINA" :			$estado = "SC";	break;
            case "SERGIPE" :				$estado = "SE";	break;
            case "SÃO PAULO" :				$estado = "SP";	break;
            case "TOCANTÍNS" :				$estado = "TO";	break;
        }

        return $estado;
    }
}
