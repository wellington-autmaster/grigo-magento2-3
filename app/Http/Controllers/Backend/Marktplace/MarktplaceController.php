<?php

namespace App\Http\Controllers\Backend\Marktplace;
use App\Helpers\Backend\Marktplace\HelperOpencart;
use App\Helpers\Backend\Marktplace\HelperMagento23;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateMarktplaceRequest;
use App\Models\Empresa\Configuracao;
use App\Models\Marktplace\Marktplace;
use App\Models\Marktplace\OcPedidosStatus;
use App\Models\Marktplace\SSProdutosEcommerce;
use App\Helpers\Backend\Marktplace\HelperPecaaqui;
use App\Helpers\Backend\Marktplace\HelperPluggTo;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Guzzle\Http\Exception\ClientErrorResponseException;


class MarktplaceController extends Controller
{
    public function index()
    {
        //Mostra os marktplaces disponiveis por empresa
        $marktplaces = Marktplace::where('empresa', Auth()->user()->empresa_codigo)->get();
        return view('backend.marktplace.marktplace.index', compact('marktplaces'));
    }

    public function editar($id)
    {
        //edita a configuração de um marktplace
        $empresa = Auth()->user()->empresa_codigo;
        $marktplace = Marktplace::where('id', $id)->where('empresa', $empresa)->first();
        $configs = Configuracao::where('modulo', $marktplace->codigo)->where('empresa', $empresa)->get();
        $status_mkt = $marktplace->status;


        if (($status_mkt == true) && ($marktplace->codigo == 'opencart')) {
            $oc_status_pedidos = $this->getStatusOc($marktplace);
            $marktplace->status_pedidos_ecom = $oc_status_pedidos;
        }


        foreach ($configs as $config) {
            switch ($config->configuracao) {
                case 'enviar_promocao' :
                    $marktplace->enviar_promocao = $config->valor;
                    break;
                case 'marca_na_descricao' :
                    $marktplace->marca_na_descricao = $config->valor;
                    break;
                case 'atualizar_descricao' :
                    $marktplace->atualizar_descricao = $config->valor;
                    break;
                case 'inativa_produto_sem_estoque' :
                    $marktplace->inativa_produto_sem_estoque = $config->valor;
                    break;
                case 'status_pedido_importar' :
                    // Recupera status configurado no opencart
                    $marktplace->status_pedido_importar = $config->valor;
                    break;
                case 'status_pos_importa_pedido' :
                    // Recupera status configurado no opencart
                    $marktplace->status_pos_importa_pedido = $config->valor;
                    break;
                case 'limite_credito_cliente' :
                    // Recupera status configurado no opencart
                    $marktplace->limite_credito_cliente = $config->valor;
                    break;
                case 'atualiza_cliente_ss' :
                    // Recupera status configurado no opencart
                    $marktplace->atualiza_cliente_ss = $config->valor;
                    break;
                case 'status_pos_nfe_pedido' :
                    // Recupera status configurado no opencart
                    $marktplace->status_pos_nfe_pedido = $config->valor;
                    break;
                case 'forma_pagamento_ss' :
                    // Recupera forma de pagamento cofigurada no ss
                    $marktplace->forma_pagamento_ss = $config->valor;
                case 'classe_nao_enviar' :
                    // Classe ABC para não enviar ao e-commerce
                    $marktplace->classe_nao_enviar = $config->valor;
                    break;
            }
        }

        // Só edita se estiver na empresa correta, senão volta para index
        if ($marktplace) {
            return view('backend.marktplace.marktplace.edit', compact('marktplace'));
        } else {
            return redirect()->route('admin.marktplace')->withFlashWarning('Ops.. Você deve editar o registro da empresa logada.');
        }
    }


    public function update(UpdateMarktplaceRequest $request)
    {
        /**Atualiza a configuração de um marktplace
         * LEGENDA
         * codigo  = plugto
         * codigo = pecaqui
         */
        $marktplace = Marktplace::find($request->id);
        $marktplace->usuario = $request->usuario;
        $marktplace->senha = $request->senha;
        $marktplace->status = (isset($request->status) ? true : false);
        $marktplace->url = $request->url;



        //Campos extras do PlugTo. (Não tem na view dos demais)
        if (($marktplace->codigo == 'plugto') OR ($marktplace->codigo == 'magento23')){
            $marktplace->client_id = $request->client_id;
            $marktplace->client_secret = $request->client_secret;
            $marktplace->save();
        }



        //CONFIGURAÇÕES OPENCART

        if ($marktplace->codigo == 'opencart') {

            //Promoções
            $config = Configuracao::where('empresa', $marktplace->empresa)->where('modulo', 'opencart')->where('configuracao', 'enviar_promocao')->first();
            $config->valor = $request->enviar_promocao == true ? true : false;
            $config->save();

            //Marca na descrição
            $config = Configuracao::where('empresa', $marktplace->empresa)->where('modulo', 'opencart')->where('configuracao', 'marca_na_descricao')->first();
            $config->valor = $request->marca_na_descricao == true ? true : false;
            $config->save();

            // Atualizar descrição (Aplicação)
            $config = Configuracao::where('empresa', $marktplace->empresa)->where('modulo', 'opencart')->where('configuracao', 'atualizar_descricao')->first();
            $config->valor = $request->atualizar_descricao == true ? true : false;
            $config->save();

            // Inativar produtos sem estoque
            $config = Configuracao::where('empresa', $marktplace->empresa)->where('modulo', 'opencart')->where('configuracao', 'inativa_produto_sem_estoque')->first();
            $config->valor = $request->inativa_produto_sem_estoque == true ? true : false;
            $config->save();


            // Status do pedido para importar ao ssplus
            $config = Configuracao::where('empresa', $marktplace->empresa)->where('modulo', 'opencart')->where('configuracao', 'status_pedido_importar')->first();
            $config->valor = (isset($request->status_pedido_importar) ? $request->status_pedido_importar : 1);
            $config->save();

            // Status do pedido  no e-commerce após importar ao ssplus
            $config = Configuracao::where('empresa', $marktplace->empresa)->where('modulo', 'opencart')->where('configuracao', 'status_pos_importa_pedido')->first();
            $config->valor = (isset($request->status_pos_importa_pedido) ? $request->status_pos_importa_pedido : 1);
            $config->save();


            // Status do pedido no e-commerce apos gerar a nota fiscal
            $config = Configuracao::where('empresa', $marktplace->empresa)->where('modulo', 'opencart')->where('configuracao', 'status_pos_nfe_pedido')->first();
            $config->valor = (isset($request->status_pos_nfe_pedido) ? $request->status_pos_nfe_pedido : 1);
            $config->save();

            // Limite de credito ao importar cliente
            $config = Configuracao::where('empresa', $marktplace->empresa)->where('modulo', 'opencart')->where('configuracao', 'limite_credito_cliente')->first();
            $config->valor = (isset($request->limite_credito_cliente) ? $request->limite_credito_cliente : '0.00');
            $config->save();

            // Atualizar cliente no ssplus com base no cadastro do e-commerce
            $config = Configuracao::where('empresa', $marktplace->empresa)->where('modulo', 'opencart')->where('configuracao', 'atualiza_cliente_ss')->first();
            $config->valor = $request->atualiza_cliente_ss == true ? true : false;
            $config->save();

            // Forma de pagamento e-commerce
            $config = Configuracao::where('empresa', $marktplace->empresa)->where('modulo', 'opencart')->where('configuracao', 'forma_pagamento_ss')->first();
            $config->valor = (isset($request->forma_pagamento_ss) ? $request->forma_pagamento_ss : '0.00');
            $config->save();

            // Forma de pagamento e-commerce
            $config = Configuracao::where('empresa', $marktplace->empresa)->where('modulo', 'opencart')->where('configuracao', 'classe_nao_enviar')->first();
            $config->valor = (isset($request->classe_nao_enviar) ? $request->classe_nao_enviar : '0.00');
            $config->save();

            //Envia CNPJ para API de e-commerce
           // $cnpj = new HelperOpencart();
           // $cnpj->configuraCNPJ($marktplace);

        }
        //FIM CONFIGURAÇÃO OPENCART



        //CONFIGURAÇÃO MAGENTO2.3
        if ($marktplace->codigo == 'magento23') {


            //Promoções
            $config = Configuracao::where('empresa', $marktplace->empresa)->where('modulo', 'magento23')->where('configuracao', 'enviar_promocao')->first();
            $config->valor = $request->enviar_promocao == true ? true : false;
            $config->save();

            //Marca na descrição
            $config = Configuracao::where('empresa', $marktplace->empresa)->where('modulo', 'magento23')->where('configuracao', 'marca_na_descricao')->first();
            $config->valor = $request->marca_na_descricao == true ? true : false;
            $config->save();

            // Atualizar descrição (Aplicação)
            $config = Configuracao::where('empresa', $marktplace->empresa)->where('modulo', 'magento23')->where('configuracao', 'atualizar_descricao')->first();
            $config->valor = $request->atualizar_descricao == true ? true : false;
            $config->save();

            // Inativar produtos sem estoque
            $config = Configuracao::where('empresa', $marktplace->empresa)->where('modulo', 'magento23')->where('configuracao', 'inativa_produto_sem_estoque')->first();
            $config->valor = $request->inativa_produto_sem_estoque == true ? true : false;
            $config->save();


            // Status do pedido para importar ao ssplus
            $config = Configuracao::where('empresa', $marktplace->empresa)->where('modulo', 'magento23')->where('configuracao', 'status_pedido_importar')->first();
            $config->valor = (isset($request->status_pedido_importar) ? $request->status_pedido_importar : 1);
            $config->save();

            // Status do pedido  no e-commerce após importar ao ssplus
            $config = Configuracao::where('empresa', $marktplace->empresa)->where('modulo', 'magento23')->where('configuracao', 'status_pos_importa_pedido')->first();
            $config->valor = (isset($request->status_pos_importa_pedido) ? $request->status_pos_importa_pedido : 1);
            $config->save();


            // Status do pedido no e-commerce apos gerar a nota fiscal
            $config = Configuracao::where('empresa', $marktplace->empresa)->where('modulo', 'magento23')->where('configuracao', 'status_pos_nfe_pedido')->first();
            $config->valor = (isset($request->status_pos_nfe_pedido) ? $request->status_pos_nfe_pedido : 1);
            $config->save();

            // Limite de credito ao importar cliente
            $config = Configuracao::where('empresa', $marktplace->empresa)->where('modulo', 'magento23')->where('configuracao', 'limite_credito_cliente')->first();
            $config->valor = (isset($request->limite_credito_cliente) ? $request->limite_credito_cliente : '0.00');
            $config->save();

            // Atualizar cliente no ssplus com base no cadastro do e-commerce
            $config = Configuracao::where('empresa', $marktplace->empresa)->where('modulo', 'magento23')->where('configuracao', 'atualiza_cliente_ss')->first();
            $config->valor = $request->atualiza_cliente_ss == true ? true : false;
            $config->save();

            // Forma de pagamento e-commerce
            $config = Configuracao::where('empresa', $marktplace->empresa)->where('modulo', 'magento23')->where('configuracao', 'forma_pagamento_ss')->first();
            $config->valor = (isset($request->forma_pagamento_ss) ? $request->forma_pagamento_ss : '0.00');
            $config->save();
        }
        //fim configuração magento2.3



        return redirect()->route('admin.marktplace')->withFlashSuccess('Configuração da API atualizada com sucesso.');
    }



    public function processarFila()
    {

        Log::info('Solicitação manual de atualização de produtos do e-Commerce.');

        shell_exec('php ../artisan marktplace:opencart > /dev/null &');
        shell_exec('php ../artisan marktplace:pluggto > /dev/null &');
        shell_exec('php ../artisan marktplace:magento > /dev/null &');
        return back()->with('status', 'Os produtos estão sendo enviados para fila de processamento.');
    }

    //PECAQUI
    public function enviaProdutosPecaAqui()
    {
        $pecaqui = new HelperPecaaqui;
        $resultado = $pecaqui->enviaProdutosPecaAqui(1, '');

        return $resultado;
    }

    //PLUGG.TO
    public function enviaProdutosPluggTo()
    {
        /** Tipos de atualização
         * 0 - Completa
         * 1 - Diaria
         * 2 - Por produto
         * (Tipo,Produto)
         */
        $pluggto = new HelperPluggTo;
        $resultado = $pluggto->enviaProdutosPlugto(1, '005864');

        return $resultado;

    }

    //ATUALIZAÇÃO DE PRODUTO INDIVIDUAL
    public function atualizaProdutoMarktplace($produto)
    {
        /** Tipos de atualização
         * 0 - Completa
         * 1 - Diaria
         * 2 - Por produto
         */

        // Opencart
        $empresas = Marktplace::Empresas('opencart');

        foreach ($empresas as $empresa) {
            try {
                Marktplace::OpencartToken($empresa->empresa);   //  Armazena o token no cache
                //Atualiza o produto em todos as empresas
                $opencart = new OpencartController();
                $opencart->enviaProdutosOpencart($empresa, $produto);

            } catch (\GuzzleHttp\Exception\ClientException $e) {
                Log::error($e->getResponse()->getStatusCode() . ' - ' . $e->getResponse()->getReasonPhrase() . ' - Opencart - Produto não atualizou - ' . $produto . "\n\n\n" . $e);
            };

            /*// Opencart Imagem
            try {
                $opencart = new HelperOpencart();
                $opencart->ProdutosImagem($empresa, $produto);

            } catch (\GuzzleHttp\Exception\ClientException $e) {
                Log::error($e->getResponse()->getStatusCode() . ' - ' . $e->getResponse()->getReasonPhrase() . ' - Opencart - Imagem do produto não atualizou - ' . $produto . "\n\n\n" . $e);
            };*/
        }


        // Magento
        $empresas = Marktplace::Empresas('magento23');

        foreach ($empresas as $empresa) {
            try {

                //Atualiza o produto em todos as empresas
                $magento = new HelperMagento23();
                $magento->enviaProdutosMagento23($empresa, $produto);



            } catch (\GuzzleHttp\Exception\ClientException $e) {
                Log::error($e->getResponse()->getStatusCode() . ' - ' . $e->getResponse()->getReasonPhrase() . ' - Magento - Produto não atualizou - ' . $produto . "\n\n\n" . $e);
            };

        }

        // Plugg.To
        $empresas = Marktplace::Empresas('plugto');
        foreach ($empresas as $empresa) {
            try {
                $pluggto = new HelperPluggTo;
                $pluggto->enviaProdutosPlugto($empresa, $produto);
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                if ($e->getResponse()->getStatusCode() == 400) {
                    Log::info('Plugg.to - Produto sem modificações -  ' . $produto->codigo_interno);
                } else {
                    Log::error($e->getResponse()->getStatusCode() . ' - ' . $e->getResponse()->getReasonPhrase() . ' - Plugg.to - Produto não atualizou - ' . $produto . "\n\n\n" . $e);
                }
            };
        }



       /* // Peça aqui
        try {
            $pecaqui = new HelperPecaaqui;
            $pecaqui->enviaProdutosPecaAqui(2, $produto);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            Log::error($e->getResponse()->getStatusCode() . ' - ' . $e->getResponse()->getReasonPhrase() . ' - Peça Aqui - Produto não atualizou - ' . $produto . "\n\n\n" . $e);
        };*/

        return back();
    }


    //AJUSTA QUEBRA DE LINHA DA APLICAÇÃO DO PRODUTO
    public function aplicacaoRemover()
    {
        $produto = SSProdutosEcommerce::where('codigo_interno', '006175')->first();
        //return dd($produto->aplicacao);
        $produto->aplicacao = str_replace("\r", "<br>", $produto->aplicacao);
        return $produto->aplicacao . '<br><br>' . $produto->aplicacao;
    }


    // SEM FUNÇÃO
    public function OcCallback(Request $request)
    {
        //testa envio de similares
        $atributos = new HelperOpencart();
        $atributos = $atributos->ProdutosAtributos();
        return dd($atributos);
        //fim envio de similares

        //  //testa envio de similares
        $similares = new HelperOpencart();
        $similares = $similares->ProdutosSimilares();
        return dd($similares);
        //fim envio de similares


        //testa envio de imagens
        $imagens = new HelperOpencart();
        $imagens = $imagens->ProdutosImagem();
        return dd($imagens);
        //fim envio de imagens


        // Testa envio de produtos
        $produtos = new HelperOpencart();
        $produtos = $produtos->enviaProdutosOpencart();
        return dd($produtos);
        //fim envio de produtos

        $opencart = Marktplace::where('nome', 'e-Commerce')->where('empresa', auth()->user()->empresa_codigo)->first();
        //  return dd($opencart);
        $http = new Client();

        $response = $http->post(parse_url($opencart->url, PHP_URL_SCHEME) . '://api.' . parse_url($opencart->url, PHP_URL_HOST) . '/oauth/token', [
            'form_params' => [
                'grant_type' => 'client_credentials',
                'client_id' => $opencart->client_id,
                'client_secret' => $opencart->client_secret,
                'scope' => '*',
            ],
        ]);

        return dd(json_decode((string)$response->getBody(), true));
    }

    public function getStatusOc(Marktplace $empresa)
    {

        $status = OcPedidosStatus::getPedidos($empresa);
        $arr_status = array();

        foreach ($status as $s) {
            $arr_status[(string)$s->order_status_id] = $s->name;
        }
        $arr_status[9999] = 'Desabilitado';
        return $arr_status;

    }


}//class
