<?php

namespace App\Http\Controllers\Backend\Marktplace\Produto;

use App\Http\Controllers\Controller;
use App\Models\Marktplace\ProdutoVeiculo;
use App\Models\Marktplace\SSProdutos;
use Illuminate\Http\Request;

class ProdutoController extends Controller
{
    public function cadastraVeiculo(Request $request)
    {
        /*Cadastra os veiculos relacionado ao produto*/

        /* Função: postVeiculoAnos
         * Componente: SelecionaVeiculos.vue
         * Dados: Veiculo, Marca, Ano, Produto, tipo  */

        //Pega todos os similares do produto
        $Similares = SSProdutos::where('codigo', $request->produto)->select('codigo', 'similarm')->first();


        //Cadastra o veiculo em todos os similares
        foreach ($Similares->similares()->get(['codigo']) as $similar){
            $produtoVeiculo = new ProdutoVeiculo();
            $produtoVeiculo->ssproduto_id = $similar->codigo;
            $produtoVeiculo->tipo_id = $request->tipo;
            $produtoVeiculo->marca_id = $request->marca;
            $produtoVeiculo->veiculo_id = $request->veiculo;
            $produtoVeiculo->ano_id = $request->ano_id;
            $produtoVeiculo->save();
        }
        return response('Veiculo cadastrado em todos os similares.', 200);

    }

    public function getAnosCadastrados($produto)
    {
        $anos = ProdutoVeiculo::where('ssproduto_id',$produto)->with('ano')->get();
        return response($anos, 200);
    }

    public function deleteVeiculo($idano,$idproduto)
    {
        //Pega todos os similares do produto
        $Similares = SSProdutos::where('codigo', $idproduto)->select('codigo', 'similarm')->first();

        foreach ($Similares->similares()->get(['codigo']) as $similar) {

            $veiculo = ProdutoVeiculo::where('ssproduto_id',$similar->codigo)->where('ano_id',$idano);
            $veiculo->delete();
        }

        return response('Veiculo eliminado de todos os similares com sucesso', 200);

    }

   
}
