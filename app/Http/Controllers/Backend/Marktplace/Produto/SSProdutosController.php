<?php

namespace App\Http\Controllers\Backend\Marktplace\Produto;

use App\Http\Controllers\Controller;
use App\Models\Marktplace\SSProdutos;
use App\Models\Marktplace\VeiculoTipo;
use App\Models\Marktplace\SSProdutosEcommerce;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SSProdutosController extends Controller
{

    Public function produtosPesquisa(Request $request)
    {
        $search = strtoupper($request->search);
        $produtos = SSProdutos::where(function ($query) use ($search) {
            $query->where('codigo', $search)
                ->orwhere('basico', $search)
                ->orwhere('marca', 'LIKE', '%' . $search . '%')
                ->orwhere('descr1', 'LIKE', $search . '%');
        })->Paginate(15);
        return view('backend.marktplace.produtos.index', compact('produtos', 'search'));

    }

    public function SSProdutosEditar($codigo)
    {
        $produto = SSProdutos::where('codigo',$codigo)->first();
        $veiculosTipo = VeiculoTipo::all();
        return view('backend.marktplace.produtos.editar',compact('produto','veiculosTipo'));
    }


    // Exibe imagem dos produto do marktplace
    public function getImagemProduto($empresa,$foto_numero,$produto)
    {
        //Gerecebe os parametros no link e consuta a imagem do produto de e-commerce.
        $imagemProduto = SSProdutosEcommerce::where('codigo_empresa',$empresa)
            ->where('codigo_interno',$produto)
            ->select('foto_1','foto_2','foto_3','foto_4')
            ->first();
;
            //Imagem -- Captura os dados binarios do postgres e converte em string 
            if(isset($imagemProduto->$foto_numero)){
                $my_bytea = stream_get_contents($imagemProduto->$foto_numero);
                $my_string = pg_unescape_bytea($my_bytea);
                $html_data = htmlspecialchars($my_string); 

                $img = base64_decode($html_data);

                return response($img,200)->header('Content-Type', 'image/jpeg');             
            };
           
        return null;
    }

    public function ImagemProduto($foto_ordem,$produto)
    {
        //Gerecebe os parametros no link e consuta a imagem do produto de e-commerce.
        $imagemProduto = DB::connection('pgsql')->table('imagem_produto')->where('ordem',$foto_ordem)->where('codigo', $produto)->first();



        //Imagem -- Captura os dados binarios do postgres e converte em string
        if(isset($imagemProduto->arquivo)){
            $my_bytea = stream_get_contents($imagemProduto->arquivo);
            $my_string = pg_unescape_bytea($my_bytea);
            $html_data = htmlspecialchars($my_string);

            $img = base64_decode($html_data);
            return response($img,200)->header('Content-Type', 'image/jpeg');
        }else{

          //  return  Storage::download('/storage/uploads/logo_empresa_monitor.png');
            return Storage::disk('public')->download('/uploads/logo_empresa_monitor.png');
        };


    }


}
