<?php

namespace App\Http\Controllers\Backend\Marktplace\Produto;

use App\Http\Controllers\Controller;
use App\Models\Marktplace\Marktplace;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ImgSSplusController extends Controller
{
    public static function ProdutosImagem($produto)
    {
        // Pesquisa imagens do produto no banco de dados
        $imagens = DB::connection('pgsql')->table('imagem_produto')->where('codigo', $produto)->get();

       /* $ultima_img = DB::connection('pgsql')->table('imagem_produto')->select('ordem')
            ->where('codigo', $produto)
            ->orderBy('ordem', 'desc')
            ->first();
       */

        $arr_imagens = [];

        foreach ($imagens as $imagem) {
            //Ajusta codificação da imagem
            $my_bytea = stream_get_contents($imagem->arquivo);
            $my_string = pg_unescape_bytea($my_bytea);
            $html_data = htmlspecialchars($my_string);

            ImgSSplusController::createImageFromBase64( ['product_id' => $produto, 'ordem' => $imagem->ordem, 'imagem' => $html_data,]);

            $ss_img = [
                'product_id' => $produto,
                'ordem' => $imagem->ordem,
                'url' => 'storage/catalog/image_'.$produto.'-'.$imagem->ordem.'.jpg'
            ];

            array_push($arr_imagens, $ss_img);
        }


        return $arr_imagens;
    }


    public static function createImageFromBase64($img)
    {
        $file_data       = $img['imagem'];

        //Ajusta o endereço e  nome da imagem
        $img_name[0] = '/catalog/image_'.$img['product_id'].'-'.$img['ordem'].'.jpg';

        if($file_data!=""){
            // Salva a imagem no servidor local
            \Storage::disk('public')->put($img_name[0],base64_decode($file_data));
        }
    }
}
