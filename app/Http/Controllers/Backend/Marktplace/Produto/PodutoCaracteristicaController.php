<?php

namespace App\Http\Controllers\Backend\Marktplace\Produto;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Marktplace\Caracteristica;
use App\Models\Marktplace\ProdutoCaracteristica;
 

class PodutoCaracteristicaController extends Controller
{
    //

    public function getCaracteristicas($descricao)
    {
        //Recebe todas as caracteristicas cadastradas no autocomplete.
        //Obs: Não sao caracteristicas vinculadas aos produtos.
        $caracteristicas = Caracteristica::where('descricao','LIKE', $descricao.'%')->get();
        return response($caracteristicas,200); 
    }

    public function getCaracteristicasProdutos($ssproduto)
    {
        //Retorna todas as caracteristicas cadastradas no produto
        $caracteristicas = ProdutoCaracteristica::where('ssproduto_id',$ssproduto)
            ->with('caracteristica')->get();

        return response($caracteristicas, 200);
    }

    public function CadastraCaracteristica(Request $request)
    {
        //Valida se a descrição da caracteristica veio com dados
        $request->validate([
            'descricao' => 'required|max:255|min:2',
            'ssproduto' => 'required|max:9|min:6',
            'valor'     => 'required|min:1|Max:200',
        ]);
        //Pesquisa se a caracteristica ja existe, senao ela é criada
        $caracteristica = Caracteristica::where('descricao', $request->descricao)->first();

        if(!$caracteristica){
            $caracteristica = new Caracteristica();
            $caracteristica->descricao = $request->descricao;
            $caracteristica->save();            
        }

        //Pesquisa se a Caracteristica ja esta no produto, se estiver é atualizada

        $caracteristricaProduto = ProdutoCaracteristica::where('caracteristica_id', $caracteristica->id)
        ->where('ssproduto_id', $request->ssproduto)->first();
        
        if($caracteristricaProduto){
            $caracteristricaProduto->valor = $request->valor;
            $caracteristricaProduto->update();

            return response($caracteristricaProduto,200);
        } 
        
        else {
        
            // Vincula a caracteristica a um produto ProdutoCaracteristica().
        $produtoCaracteristica = new ProdutoCaracteristica();
        $produtoCaracteristica->ssproduto_id = $request->ssproduto;
        $produtoCaracteristica->caracteristica_id = $caracteristica->id;
        $produtoCaracteristica->valor = $request->valor;
        $produtoCaracteristica->save();

        return response($caracteristica,200);

        }       
    }

    public function deletaCaracteristicaProduto($id)
    {
        if($id){
            $caracteristica = ProdutoCaracteristica::find($id);
            $caracteristica->delete();
        }
        return response("Caracteristica deletada com sucesso", 200);
    }

 
}
