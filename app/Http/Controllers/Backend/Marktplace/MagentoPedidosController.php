<?php

namespace App\Http\Controllers\Backend\Marktplace;

use App\Jobs\ImportaPedidosMagento;
use App\Jobs\jobAtualizaPedidoMagento;
use App\Models\Empresa\Configuracao;
use App\Models\Marktplace\MagentoPedidos;
use App\Models\Marktplace\Marktplace;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;





class MagentoPedidosController extends Controller
{
    public $homologacao = '';

    public function __construct()
    {
        if( config('magento.homologacao') ) {
            $this->homologacao = '/homolog/';
        }
    }

    public static function getPedidos()
    {
        Log::info('Importação de pedidos iniciado Magento');
        /** @var  $empresas - Todos os marktplaces ativos do Opencart
        Verifica se existem pedidos com o status configurado a importar*/
        $empresas = Marktplace::Empresas('magento23');
        $status_pedido_processando = config('magento.status_para_importar');


        foreach ($empresas as $empresa){
            $pedidos = MagentoPedidos::getPedidosMagento($empresa, $status_pedido_processando);


            foreach ($pedidos->items as $pedido){

                ImportaPedidosMagento::dispatch($empresa, $pedido)->onQueue('default');

            }
        }

        return Log::notice('Pedidos enviados para fila de processamento');
    }

    /**
     * @param Marktplace $empresa
     * @param $pedido
     * @return bool
     */
    public function importaPedidosSSPlus(Marktplace $empresa, $pedido)
    {
        if (config('app.env') == 'local') {
            Log::debug('cheguei no importaPedidosSSplusMarketplace');
        }

        $url_gestao = config('apigestaoparts.url_api');
        $username = config('apigestaoparts.username');
        $password = config('apigestaoparts.password');

        $token_gestao = $this->ApiGestaoToken($url_gestao, $username, $password);


        // Configura dados do cliente e envia ao post. Retorna o CPF
        $cpfcnpj = $this->postCliente($empresa, $pedido, $token_gestao);



        $forma_pag_ss = Configuracao::getConfigMagento23($empresa, 'forma_pagamento_ss');
        $data_magento = substr($pedido->created_at, 0, 10);
        $data = implode("/", array_reverse(explode("-", $data_magento)));
        $hora = substr($pedido->created_at, 11);
        //Prepara informações do pedido no produto e trata observação
        $obs = $pedido->shipping_description . ' R$' . $pedido->shipping_amount;
        $obs = $obs . ' | PED. ' . (String)$pedido->entity_id;

        $observacao = substr($obs, 0, 500);
        $operacao = config('magento.operacao');

        $itens = [];

        foreach ($pedido->items as $produto) {

            $sku = substr($produto->sku, 0, 6);
            $numero_pedido = str_pad($pedido->entity_id, 6, '0', STR_PAD_LEFT);

            $produto = [
                'codigoerp' => $sku,
                'quanti' => $produto->qty_ordered,
                'unitario' => $produto->price
            ];

            array_push($itens, $produto);
        }



        $arr_produto = [
            'empresa' => $empresa->empresa,
            'pedido' => $numero_pedido,
            'operacao' => $operacao,
            'docoriginal' => $pedido->entity_id,
            'data' => $data,
            'hora' => $hora,
            'cpf_cnpj' => $cpfcnpj,
            'codforpag' => $forma_pag_ss,
            'codcondpag' => '',
            'observ' => $observacao,
            'tipoentrega' => '2',
            'itens' => $itens,
        ];




        $url_api = config('apigestaoparts.url_api');
        $arr_produto = json_encode($arr_produto);
        Log::debug('Arr produto: '. $arr_produto);
        $headers = [
            'content-type' => 'application/json',
            'accept' => 'application/json',
            'Authorization' => 'Bearer ' . $token_gestao
        ];
        $array = ['body' => $arr_produto, 'headers' => $headers];

        try {
            // Create a client with a base URI
            $client = new Client(['base_uri' => $url_api]);
            $response = $client->request('POST', '/erpssplus/pedido/', $array);
            Log::info($empresa->empresa. ' MagentoPedidosController: ' . $response->getBody());

            if($response->getStatusCode() == '200') {
                Log::notice('Pedido AutoBitts: '. $pedido->entity_id. ' Importado com sucesso!');
                $novo_status = config('magento.status_apos_importar');
                $comentario = 'Seu pedido está em processo de separação. A separação de pedidos é a etapa intermediaria entre a aprovação e o faturamento do pedido.';
                jobAtualizaPedidoMagento::dispatch($empresa, $pedido->entity_id, $novo_status, $comentario)->onQueue('default');
            }

        } catch (ClientException $e) {
            Log::error($empresa->empresa. 'MagentoPedidosController, Pedido: '. $pedido->entity_id  .' Ocorrem problemas com a criação do pedido no SSPlus - ' . $e->getMessage()  ,[$arr_produto]);
        };


    }


    public function getClienteDoPedido($pedido)
    {
        $url_api = config(env('magento.url_magento'));

        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => '*/*'
        ];

        $stack = HandlerStack::create();
        $oauth = new Oauth1(config('magento.Oauth1'));
        $stack->push($oauth);

        $client = new Client([
            'base_uri' => $url_api,
            'headers' => $headers,
            'handler' => $stack,
        ]);


        $customer = $client->get($this->homologacao.'/rest/V1/customers/'. $pedido->customer_id, ['auth' => 'oauth']);
        $customer = json_decode($customer->getBody());


        return $customer;
    }

    public function postCliente(Marktplace $empresa, $pedido, $token)
    {
        Log::debug(json_encode($pedido));
        if (config('app.env') == 'local') {
            Log::debug('Cheguei no postCliente();');
        }



       //verificações ----------
       // define o limite de crédito de acordo com as configurações do marketplace
       //$limite_credito = (string)round(Configuracao::getConfigMagento23($empresa, 'limite_credito_cliente'));
       //verifica se é para atualizar cadastro do cliente no SSPlus
        Configuracao::getConfigMagento23($empresa, 'atualiza_cliente_ss') == true ? $atualiza_cliente = 'S' : $atualiza_cliente = 'N';
        //forma o nome completo
        $nome_sobrenome = $pedido->billing_address->firstname . ' ' . $pedido->billing_address->lastname;
        $nome_completo = substr($nome_sobrenome, 0, 40);
        //valida cep
        $cep = str_replace("-", "", $pedido->billing_address->postcode);
        //define estado e verifica dados do endereço
        $estado = $pedido->billing_address->region_code;
        $cidade = $pedido->billing_address->city;
        $email = $pedido->billing_address->email;
        $endereco = $pedido->billing_address->street;
        $ie_rg = '';

        /*   if ($cliente_do_pedido->custom_attributes->ie_rg){
             $ie_rg =  $cliente_do_pedido->custom_attributes->ie_rg;
           }else{
               $ie_rg = '';
           }
      */

        array_key_exists(0, $endereco) ? $rua = $endereco[0] : $rua = false;
        array_key_exists(1, $endereco) ? $numero = $endereco[1] : $numero = false;
        array_key_exists(2, $endereco) ? $complemento = $endereco[2] : $complemento = false;
        array_key_exists(3, $endereco) ? $bairro = $endereco[3] : $bairro = false;

        if (!$rua || !$numero || !$bairro) {
            $array_campos = [];
            $rua ?: $array_campos[] = "Rua";
            $numero ?: $array_campos[] = "Número";
            $bairro ?: $array_campos[] = "Bairro";

            Log::error("Endereço incompleto no pedido :" . $pedido->entity_id . " Verifique o(s) campo(s): ", $array_campos);
            exit('Endereço incompleto');
        }


        //valida telefone
        $pedido->billing_address->telephone == null ?
            $telefone = "00999999999" :
            $telefone = preg_replace("/[^0-9]/", "", $pedido->billing_address->telephone);


        $cpfcnpj = preg_replace("/[^0-9]/", "", $pedido->billing_address->vat_id);

        if (!$cpfcnpj) {
            Log::error('CPF/CNPJ Inválido ' . 'CPF/CNPJ ' . $pedido->billing_address->vat_id . 'Pedido: ' . $pedido->entity_id);
            exit('CPF/CNPJ Inválido');
        }
        //verifia se é PF OU PJ
        $contagem = strlen($cpfcnpj);
        $contagem == '11' ? $PF = true : $PF = false;
        $contagem == '14' ? $PJ = true : $PJ = false;

        //fim das verificações ---------------


        if ($PF == true){
            $arrusuario = [
                'nome'    => $nome_completo,
                'fantasia'  => '',
                'cpf_cnpj' => $cpfcnpj,
                'cep' => $cep,
                'endereco' => $rua,
                'numero' => $numero,
                'complemento' => $complemento,
                'bairro' => $bairro,
                'cidade' => $cidade,
                'estado' => $estado,
                'fone' => $telefone,
                'celular' => $telefone,
                'email' => $email,
                'inscricaoie' => 'ISENTO',
                'rg' => $ie_rg
            ];
        }elseif($PJ == true){
            $arrusuario = [
                'nome'    => $nome_completo,
                'fantasia'  => $pedido->customer_lastname,
                'cpf_cnpj' => $cpfcnpj,
                'cep' => $cep,
                'endereco' => $rua,
                'numero' => $numero,
                'complemento' => $complemento,
                'bairro' => $bairro,
                'cidade' => $cidade,
                'estado' => $estado,
                'fone' => $telefone,
                'celular' => $telefone,
                'email' => $email,
                'inscricaoie' => $ie_rg,
                'rg' => ''
            ];

        }else{
            Log::emergency('CPF/CNPJ está cadastrado de forma incorreta ou não existe. Pedido: '. $pedido->entity_id. 'Cliente: '. $nome_completo);
        }


        $url_api = config('apigestaoparts.url_api');
        $arrusuario = json_encode($arrusuario);
        Log::debug('arr usuario pessoa eprsss: '. $arrusuario);
        $headers = [
            'content-type' => 'application/json',
            'accept' => 'application/json',
            'Authorization' => 'Bearer ' . $token
        ];
        $array = ['body' => $arrusuario, 'headers' => $headers];
        try {
            // Create a client with a base URI
            $client = new Client(['base_uri' => $url_api]);
            $response = $client->request('POST', '/erpssplus/pessoa/', $array);
            Log::info($empresa->empresa. ' MagentoPedidosController: ' . $response->getBody());
            return $cpfcnpj;

        } catch (ClientException $e) {
            Log::error($empresa->empresa. 'MagentoPedidosController, Pedido: '. $pedido->entity_id  .' Ocorrem problemas com a criação do usuário do SSPlus - ' . $e->getMessage()  ,[$pedido]);
        };

        return true;

    }


    public static function ApiGestaoToken($url_gestao, $username, $password)
    {

        if(config('app.env') == 'local' ) {
            Log::debug('Cheguei no ApiGestaoToken();');
        }

        $http = new Client();
        $url = $url_gestao. '/token';
        Log::debug('URL API GESTAO:'. $url);

        $response = $http->post($url_gestao.'/token', [
            'form_params' => [
                'grant_type' => 'password',
                'username' => $username,
                'password' => $password,
                'scope' => '*',
            ],
        ]);

        $api_token = json_decode((string) $response->getBody());
        Log::debug('TOKEN IGUAL A: '. $api_token->access_token);
        $api_token  = $api_token->access_token;
        // $api_token  = $api_token->token_type. ' '.$api_token->access_token;

        return $api_token;
    }
    public function statusTransacao($trans)
    {
        $codigo = explode(' ', $trans);

        switch ($codigo[0]) {
            case 'SSECOM+0004':
                $status = true;
                break;
            case 'SSECOM+0001':
                $status = true;
                break;
            case 'SSECOM+0002':
                $status = true;
                break;
            case 'SSECOM-9999':
                $status = false;
                break;
            case 'SSECOM-1048':
                $status = false;
                break;
            case 'SSECOM-7648':
                $status = false;
                break;
            case 'SSECOM-1448':
                $status = false;
                break;
            case 'SSECOM-1449':
                $status = false;
                break;
            case 'SSECOM-1347':
                $status = false;
                break;
            case 'SSECOM-1932':
                $status = false;
                break;
            case 'SSECOM-2330':
                $status = false;
                break;
            case 'SSECOM-2731':
                $status = false;
                break;
            case 'SSECOM-3147':
                $status = false;
                break;
            case 'SSECOM-1022':
                $status = false;
                break;
        }

        return $status;
    }



    public function processaFilaPedidos()
    {
        Log::info('Solicitação manual de importação de pedidos do Magento');
        shell_exec('php ../artisan magento:importa-pedidos > /dev/null &');

        return back()->with('status','Os pedidos do Magento estão sendo enviados para fila de processamento.');
    }




    public  function getEstado($estado)
    {
        switch ($estado) {
            /* UFs */
            case "AC" :	$estado = "Acre";					break;
            case "AL" :	$estado = "Alagoas";				break;
            case "AM" :	$estado = "Amazonas";				break;
            case "AP" :	$estado = "Amapá";					break;
            case "BA" :	$estado = "Bahia";					break;
            case "CE" :	$estado = "Ceará";					break;
            case "DF" :	$estado = "Distrito Federal";		break;
            case "ES" :	$estado = "Espírito Santo";		    break;
            case "GO" :	$estado = "Goiás";					break;
            case "MA" :	$estado = "Maranhão";				break;
            case "MG" :	$estado = "Minas Gerais";			break;
            case "MS" :	$estado = "Mato Grosso do Sul"; 	break;
            case "MT" :	$estado = "Mato Grosso";			break;
            case "PA" :	$estado = "Pará";					break;
            case "PB" :	$estado = "Paraíba";				break;
            case "PE" :	$estado = "Pernambuco";			    break;
            case "PI" :	$estado = "Piauí";					break;
            case "PR" :	$estado = "Paraná";				    break;
            case "RJ" :	$estado = "Rio de Janeiro";		    break;
            case "RN" :	$estado = "Rio Grande do Norte";	break;
            case "RO" :	$estado = "Rondônia";				break;
            case "RR" :	$estado = "Roraima";				break;
            case "RS" :	$estado = "Rio Grande do Sul";		break;
            case "SC" :	$estado = "Santa Catarina";		    break;
            case "SE" :	$estado = "Sergipe";				break;
            case "SP" :	$estado = "São Paulo";				break;
            case "TO" :	$estado = "Tocantíns";				break;

            /* Estados */
            case "ACRE" :					$estado = "AC";	break;
            case "ALAGOAS" :				$estado = "AL";	break;
            case "AMAZONAS" :				$estado = "AM";	break;
            case "AMAPÁ" :					$estado = "AP";	break;
            case "BAHIA" :					$estado = "BA";	break;
            case "CEARÁ" :					$estado = "CE";	break;
            case "DISTRITO FEDERAL" :		$estado = "DF";	break;
            case "ESPÍRITO SANTO" :			$estado = "ES";	break;
            case "GOIÁS" :					$estado = "GO";	break;
            case "MARANHÃO" :				$estado = "MA";	break;
            case "MINAS GERAIS" :			$estado = "MG";	break;
            case "MATO GROSSO DO SUL" :		$estado = "MS";	break;
            case "MATO GROSSO" :			$estado = "MT";	break;
            case "PARÁ" :					$estado = "PA";	break;
            case "PARAÍBA" :				$estado = "PB";	break;
            case "PERNAMBUCO" :				$estado = "PE";	break;
            case "PIAUÍ" :					$estado = "PI";	break;
            case "PARANÁ" :					$estado = "PR";	break;
            case "RIO DE JANEIRO" :			$estado = "RJ";	break;
            case "RIO GRANDE DO NORTE" :	$estado = "RN";	break;
            case "RONDÔNIA" : 				$estado = "RO";	break;
            case "RORAIMA" :				$estado = "RR";	break;
            case "RIO GRANDE DO SUL" :		$estado = "RS";	break;
            case "SANTA CATARINA" :			$estado = "SC";	break;
            case "SERGIPE" :				$estado = "SE";	break;
            case "SÃO PAULO" :				$estado = "SP";	break;
            case "TOCANTÍNS" :				$estado = "TO";	break;
        }

        return $estado;
    }


}
