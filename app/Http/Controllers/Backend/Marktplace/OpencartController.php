<?php

namespace App\Http\Controllers\Backend\Marktplace;

use App\Http\Controllers\Controller;
use App\MasterProcess;
use App\Models\Empresa\Configuracao;
use App\Models\Marktplace\Marktplace;
use App\Models\Marktplace\SSProdutosEcommerce;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class OpencartController extends Controller
{
    public function enviaProdutosOpencart($empresa, $codigoProduto)
    {

        $produto = SSProdutosEcommerce::where('codigo_interno', $codigoProduto)->where('codigo_empresa', $empresa->empresa)->first();
        $produto = $this->personalizarAplicacao($produto);
        $produto = $this->defineSecaoGrupo($produto);
        $produto = $this->validaConfiguracoes($empresa->empresa, $produto);
        $produto = $this->ativaOuInativaProduto($empresa->empresa,$produto);

        //verifica se o modulo esta ativo
        $moduloOpencart = MasterProcess::statusContrato($empresa, 'Opencart');
        Log::debug(json_encode($moduloOpencart));
        $moduloOpencart->producao ? $nomeProduto = $produto->descricao : $nomeProduto =  ' [HOMOLOGAÇÃO] '.$produto->descricao ;

        $arrprodutos[] = [
            'product_id' => trim($produto->codigo_interno),
            'model' => trim($produto->codigo_fabricante),
            'sku' => trim($produto->codigo_interno),
            'ean' => trim($produto->codigo_barras),
            'status' => trim($produto->status),
            'quantity' => trim($produto->estoque_disponivel),
            'image' => '',
            'price' => trim($produto->preco_venda),
            'weight' => trim($produto->peso_bruto) ?: 1,
            'length' => trim($produto->comprimento) ?: 1,
            'width' => trim($produto->largura) ?: 1,
            'height' => trim($produto->altura) ?: 1,
            'name' => trim($nomeProduto),
            'description' => str_replace("\r", "<br>", $produto->aplicacao),
            'price_special' => trim($produto->preco_promocao),
            'date_start' => trim($produto->data_inicio_promocao),
            'date_end' => trim($produto->data_fim_promocao),
            'codigo_secao_ext' => trim($produto->secao),
            'secao_name' => trim($produto->secao_descricao),
            'codigo_grupo_ext' => trim($produto->grupo),
            'grupo_name' => trim($produto->grupo_descricao),
            'secao_top' => 1,
            'manufacturer_name' => trim($produto->marca) ?: 'GERAL'
        ];


        $validacao = $this->validacaoProduto($arrprodutos);

        if(!$validacao) {
            return false;
        }

        $arrprodutos = json_encode($arrprodutos);

        $headers = [
            'content-type' => 'application/json',
            'accept' => 'application/json',
            'Authorization' => 'Bearer ' . Marktplace::OpencartToken($empresa->empresa)
        ];

        $array = ['body' => $arrprodutos, 'headers' => $headers];
        $url = $this->formatarUrl($empresa->url);

        try {
            // Create a client with a base URI
            $client = new Client(['base_uri' => $url]);
            $response = $client->request('POST', 'api/v1/products', $array);
            Log::info($empresa->empresa. ' Oc Produto: ' . $response->getBody());

        } catch (ClientException $error) {
            Log::error($empresa->empresa. 'Oc Produto - Ocorreram problemas com atualização do produto '. $produto->codigo_interno.' - ' . $error->getMessage()  ,[$produto]);
        };

        return true;
    }

    public function enviarAtributos($empresa , $produto)
    {
        $arr_produto = [];
        $arr_atributos = [];

        $atributos = DB::connection('pgsql')->table('produtos_ecommerce_caracteristicas')
            ->where('codigo_interno', $produto)->get();

        if ($atributos->count() > 0) {

            foreach ($atributos as $atributo) {
                $arr_atributos[] = [
                    'attribute_id' => trim($atributo->codigo_caracteristica),
                    'attribute_name' => trim($atributo->descricao_caracteristica),
                    'attribute_valor' => trim($atributo->valor_caracteristica),
                    'filtro' => trim($atributo->filtro_ecommerce) == 'S' ? true : false
                ];
            }
            $arr_produto[] = [
                'product_id' => $produto,
                'attributes' => $arr_atributos
            ];
        }

        $api_token = Marktplace::OpencartToken($empresa->empresa);
        $headers = [
            'content-type' => 'application/json',
            'accept' => 'application/json',
            'Authorization' => 'Bearer ' . $api_token
        ];
        $arr_produto = json_encode($arr_produto);

        $array = ['body' => $arr_produto, 'headers' => $headers];
        $url = $this->formatarUrl($empresa->url);

        try {
            // Create a client with a base URI
            $client = new Client(['base_uri' => $url]);
            $client->request('POST', 'api/v1/products/attributes', $array);

        } catch (ClientException $error) {
            Log::error($empresa->empresa . 'Oc Atributos: ' .$arr_produto . $error  );
        }

        return response('Atributos e filtros cadastrados', 200);

    }

    public function ativaOuInativaProduto($codigo_empresa, $produto)
    {
        //Valida se o produto esta ativo
        $produto->status = $produto->excluir_site ? false : true;

        //Inativar produto sem estoque (Configurado)
        $config = Configuracao::where('modulo','opencart')->where('configuracao','inativa_produto_sem_estoque')->where('empresa',$codigo_empresa)->first();
        if($config->valor){
            ($produto->estoque_disponivel > 0) ?: $produto->status = false;
        }

        /*        Inativar produtos de classes não permitidas*/
        // Caso o produto contenha uma classe, verificar se existem classes configuradas para não subir ao e-commerce
        $classesBloqueadas = Configuracao::where('modulo','opencart')->where('configuracao','classe_nao_enviar')->where('empresa',$codigo_empresa)->first();

        if($produto->classe_abc &&  $classesBloqueadas ) {

            //Verificar classes configuradas para não enviar ao e-commerce
            $classesNaoPermitidas = explode(';', $classesBloqueadas->valor); //O formato salvo ex: A,B,C...

            //Verifica se a classe do produto esta contido nas classes para não enviar ao e-commerce
            $classe = array_diff([$produto->classe_abc], $classesNaoPermitidas);


            if($classe !== [$produto->classe_abc]) {
                //Caso a classse esteja incluida na lista de bloqueio, desabilita o produto no e-commerce
                $produto->status = false;
            }
        }

        //Inativa produto, caso configurado para verificar foto
        if(env('APP_ECOMM_VERIFICA_FOTO')){
            $produto->foto_1 ?: $produto->status = false;
        }

        return $produto;
    }

    public function personalizarAplicacao($produto)
    {
        $produto->data_atualizacao = date_format(date_create($produto->data_atualizacao),'d/m/Y');
        $aplicacao =  "<strong>Descrição: </strong> $produto->descricao <br>" .
            "<strong>Marca: </strong> $produto->marca <br>" .
            "<strong>Código interno: </strong>  $produto->codigo_interno<br>" .
            "<strong>Código Fabricante: </strong>  $produto->codigo_fabricante<br>" .
            "<strong>Código de barras: </strong>  $produto->codigo_barras<br>  " .
            "<strong>Ultima alteração: </strong>   $produto->data_atualizacao<br><hr><br> $produto->aplicacao  "   ;
        $produto->aplicacao = $aplicacao;

        return $produto;
    }

    public function defineSecaoGrupo($produto)
    {
        if (trim($produto->descricao_secao_ecommerce) != trim($produto->secao_descricao) &&
            trim($produto->codigo_secao_ecommerce) != null &&
            trim($produto->descricao_secao_ecommerce) != null
        ) { //não deixa gravar outra seção se a descrição for igua a primcipal
            $produto->secao_descricao = trim($produto->descricao_secao_ecommerce);
            $produto->secao = (integer)'9' . $produto->codigo_secao_ecommerce;
        } else {
            $produto->secao = (integer)'8' . $produto->secao;
        }
        $produto->product_id = (integer)$produto->product_id;
        return $produto;
    }

    public function validaConfiguracoes($empresa, $produto)
    {
        //Configurações -- Valida promoção
        $config = Configuracao::where('modulo','opencart')->where('configuracao','enviar_promocao')->where('empresa',$empresa)->first();
        if(!$config->valor){
            $produto->preco_promocao = '';
            $produto->data_inicio_promocao = '';
            $produto->data_fim_promocao = '';
        };

        //marca_na_descricao
        $config = Configuracao::where('modulo','opencart')->where('configuracao','marca_na_descricao')->where('empresa',$empresa)->first();
        if($config->valor){
            $produto->descricao = $produto->descricao.' '. $produto->marca;
        }

        //atualizar_descricao
        $config = Configuracao::where('modulo','opencart')->where('configuracao','atualizar_descricao')->where('empresa',$empresa)->first();
        $produto->description_update = $config->valor;

        return $produto;

    }

    public function validacaoProduto($data)
    {
        $rules =  [
            'product_id' => 'required|max:11',
            'model' => 'required|max:64',
            'sku' => 'required|max:64',
            'ean' => 'max:14',
            'status' => '',
            'quantity' => 'min:0|max:20',
            'image' => '',
            'price' => 'max:50',
            'weight' => 'required|max:14',  /*peso_bruto*/
            'length' =>'required|max:14',  /*comprimento*/
            'width' => 'required|max:14',  /*largura*/
            'height' => 'required|max:14', /*altura*/
            'name' => 'required|max: 250',
            'description' => '',
            'price_special' => 'max:15',
            'date_start' => '',
            'date_end' => '',
            'codigo_secao_ext' => 'max:5',
            'secao_name' => 'required|max:255',
            'codigo_grupo_ext' => 'max:5',
            'grupo_name' => 'max:255',
            'secao_top' => '',
            'manufacturer_name' => 'max:64'
        ];



        $validator =    Validator::make($data[0], $rules);
        $errors = $validator->errors();

        foreach ($errors->all() as $message) {
            Log::error($message .' '. $data[0]['product_id'].' 
             ',            $data);

        }

        if ($validator->fails()) {
            return false;
        }
        return true;
    }

    public function enviarImagem($empresa, $produto)
    {
        $api_token = Marktplace::OpencartToken($empresa->empresa);   //   return $opencart['access_token'];

        $arr_produtos = [];

        $imagens = DB::connection('pgsql')->table('imagem_produto')->where('codigo', $produto)->get();

        $ultima_img = DB::connection('pgsql')->table('imagem_produto')->select('ordem')
            ->where('codigo', $produto)
            ->orderBy('ordem', 'desc')
            ->first();
        $arr_imagens = [];

        foreach ($imagens as $imagem) {
            //Ajusta codificação da imagem
            $my_bytea = stream_get_contents($imagem->arquivo);
            $my_string = pg_unescape_bytea($my_bytea);
            $html_data = htmlspecialchars($my_string);

            $arr_imagens[] = [
                'product_id' => $produto,
                'ordem' => $imagem->ordem,
                'imagem' => $html_data,
                'ultima_imagem' => $ultima_img->ordem
            ];
        }
        $arr_produtos[] = ['product_id' => $produto, 'imagens' => $arr_imagens];

        $arr_produtos = json_encode($arr_produtos);

        $headers = [
            'content-type' => 'application/json',
            'accept' => 'application/json',
            'Authorization' => 'Bearer ' . $api_token
        ];

        $array = ['body' => $arr_produtos, 'headers' => $headers];
        $url = $this->formatarUrl($empresa->url);
        // Create a client with a base URI

        try {
            $client = new Client(['base_uri' => $url]);
            $client->request('POST', 'api/v1/products/image', $array);
        } catch (ClientException $error) {
            Log::error($empresa->empresa . 'Envio da imagem: ' .$arr_produtos . $error  );
        }

        return response('Atualização de imagens finalizado com sucesso', 200);
    }

    public function formatarUrl($url)
    {
        return parse_url($url, PHP_URL_SCHEME) . '://api.' . parse_url($url, PHP_URL_HOST) . '/';

    }
}
