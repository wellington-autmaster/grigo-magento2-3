<div class="col-sm-6 col-lg-3">
    <div class="card">
        <div class="card-body">
            <div class="text-value">352</div>
            <div>Pedidos.</div>
            <div class="progress progress-xs my-2">
                <div class="progress-bar bg-info" role="progressbar" style="width: 65%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <small class="text-muted">Pedidos aguardando processamento.</small>
        </div>
    </div>
</div>

<div class="col-sm-6 col-lg-3">
    <div class="card">
        <div class="card-body">
            <div class="text-value">352</div>
            <div>Separação.</div>
            <div class="progress progress-xs my-2">
                <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <small class="text-muted">Pedidos aguardando separação.</small>
        </div>
    </div>
</div>
<div class="col-sm-6 col-lg-3">
    <div class="card">
        <div class="card-body">
            <div class="text-value">352</div>
            <div>Conferência.</div>
            <div class="progress progress-xs my-2">
                <div class="progress-bar bg-info" role="progressbar" style="width: 15%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <small class="text-muted">Pedidos aguardando conferência.</small>
        </div>
    </div>
</div>
<div class="col-sm-6 col-lg-3">
    <div class="card">
        <div class="card-body">
            <div class="text-value">352</div>
            <div>Expedição.</div>
            <div class="progress progress-xs my-2">
                <div class="progress-bar bg-info" role="progressbar" style="width: 4%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <small class="text-muted">Pedidos aguardando expedição.</small>
        </div>
    </div>
</div>