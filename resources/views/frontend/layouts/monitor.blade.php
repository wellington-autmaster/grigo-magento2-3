<!DOCTYPE html>
@langrtl
<html lang="pt-BR" dir="rtl">
@else
    <html lang="pt-BR">
    @endlangrtl
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title', app_name())</title>
        <meta name="description" content="@yield('meta_description', 'Gestão Parts -  Monitor de separação de peças.')">
        <meta name="author" content="@yield('meta_author', 'Gestão Parts')">
    @yield('meta')

    {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
    @stack('before-styles')

    <!-- Check if the language is set to RTL, so apply the RTL layouts -->
        <!-- Otherwise apply the normal LTR layouts -->
        {{ style(mix('css/frontend.css')) }}

        @stack('after-styles')
    </head>

    <body style="background-image: url({{ asset($monitor->imgBackground) }})">


    <div id="app">
        <vc-header v-bind:monitor="{{ $monitor }}" v-bind:empresa="{{$monitor->Empresa}}"></vc-header>



        <div class="container-fluid ">
            <div class="row " >
                <div class="col-12">
                    @if($monitor->senhas)
                        <vc-senhas-aguardando v-bind:monitor="{{ $monitor }}"></vc-senhas-aguardando>
                    @endif
                </div>
            </div>
            <div class="row  "  >
                <promocoes v-bind:monitor="{{ $monitor }} "></promocoes>
                @if($monitor->noticias == true && $monitor->senhas == false)
                   {{-- <vc-noticias v-bind:monitor="{{ $monitor }}"></vc-noticias>--}}
                @endif

                @if($monitor->senhas == true)
                    <vc-senhas-processamento v-bind:monitor="{{ $monitor }}"></vc-senhas-processamento>
                @endif

                @if($monitor->senhas)
                    <vc-senhas-concluido v-bind:monitor="{{ $monitor }}"></vc-senhas-concluido>
                @endif
            </div>




{{--
        <vc-noticias-footer v-bind:monitor="{{ $monitor }}"></vc-noticias-footer>
--}}


    </div>

    <div style="background-color: black; opacity: 0.9; display: flex " class="footer">
        <div class="col-2">
            <img style="max-height: 50px;  margin: 5px"
                 src="{{asset('/img/backend/marktplace/gestao-parts-logo.png')}}"/>
        </div>
        <div class="col-10">
        <vc-noticias-footer v-bind:monitor="{{ $monitor }}"></vc-noticias-footer>
        </div>
        <div >
            {{--<h4 style="margin-top: 10px" >Especialista em gestão empresarial.</h4>--}}
        </div>





    </div>
    </div>

    @stack('before-scripts')
    {!! script(mix('js/frontend.js')) !!}
    @stack('after-scripts')


    </body>
    </html>
