<!DOCTYPE html>
@langrtl
<html lang="pt-BR" dir="rtl">
@else
    <html lang="pt-BR">
    @endlangrtl
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title', app_name())</title>
        <meta name="description" content="@yield('meta_description', 'Gestão Parts Monitor de separação de peças.')">
        <meta name="author" content="@yield('meta_author', 'Gestão Parts')">
    @yield('meta')

    {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
    @stack('before-styles')

    <!-- Check if the language is set to RTL, so apply the RTL layouts -->
        <!-- Otherwise apply the normal LTR layouts -->
        {{ style(mix('css/frontend.css')) }}

        @stack('after-styles')


    </head>
    <body style="background-color: #1a2226">


    <div id="app">


        <div class="container-fluid">

            <div class="row ">
                <div class="col col-12">
                    <vc-ordem-servico empresa="{{$empresa}}"></vc-ordem-servico>
                </div>
            </div>

        </div>
    </div>

    <div style="background-color: black; opacity: 0.9" class="footer">
        <img style="max-height: 50px; float: left; margin: 5px" src="{{asset('/img/backend/marktplace/gestao-parts-logo.png')}}"/>
        <h4 style="float: right;margin: 10px">Especialista em gestão empresarial.</h4>
    </div>



    @stack('before-scripts')
    {!! script(mix('js/frontend.js')) !!}
    @stack('after-scripts')

    </body>
    </html>
