<footer class="footer text-muted">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                Logs de - <span class="label label-info">Versão {{ log_viewer()->version() }}</span>
            </div>
            <div class="col-md-6 text-right ">
                
            </div>
        </div>
    </div>
</footer>
