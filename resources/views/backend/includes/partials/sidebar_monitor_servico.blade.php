<li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/monitor/servico/'), 'open') }}">
    <a class="nav-link nav-dropdown-toggle" href="{{ route('frontend.viewservicos', Auth()->user()->empresa_codigo) }}" target="_blank">
        <i class="icon-wrench"></i> Serviços

    </a>

    <ul class="nav-dropdown-items">
        {{--
                <li class="nav-item">
                    <a class="nav-link " href="{{ route('frontend.viewservicos', Auth()->user()->empresa_codigo) }}" target="_blank">
                        Monitor
                    </a>
                </li>
                <li class="nav-item hidden">
                    <a class="nav-link hidden {{ active_class(Active::checkUriPattern('admin/monitor/servico/')) }}" href="{{ route('admin.monitor.index') }} " >
                        Configurações
                    </a>
                </li>
        --}}
    </ul>
</li>
