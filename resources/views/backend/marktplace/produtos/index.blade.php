@extends('backend.layouts.app')

@section ('title', 'Pesquisa de produtos')

@section('breadcrumb-links')
    @include('backend.marktplace.produtos.includes.breadcrumb-links')
@endsection


@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-3">
                    <h4 class="card-title mb-0">
                        Pesquisa de produtos <small class="text-muted"> SSPlus</small>

                    </h4>
                </div><!--col-->

                    <div class="col-sm-9">
                        <form id="frmPesquisaProduto" action="{{ route('admin.SSprodutosPesquisa') }}" method="get">
                            {{ csrf_field() }}
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <button class="btn btn-outline-secondary" type="submit" form="frmPesquisaProduto" ><i class="fa fa-search" aria-hidden="true"></i></button>
                            </div>
                            <input name="search" class="form-control text-uppercase" placeholder="Pesquise produtos no SSplus com: Código do fabricante, descrição ou marca" aria-label="" aria-describedby="basic-addon1">
                        </div>
                        <!--col   'backend.auth.user.includes.header-buttons' -->
                        </form>
                    </div>
                <!--col-->
            </div><!--row-->

            <div class="row mt-4">
                <div class="col">
                    @isset($produtos)
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Codigo</th>
                                    <th>Fabricante</th>
                                    <th>Marca</th>
                                    <th>Classificação</th>
                                    <th>Descrição</th>
                                    <th>Preco venda</th>
                                    <th>Estoque</th>
                                    <th>Última atualização</th>
                                    <th>Site</th>
                                    <th>Ação</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($produtos as $produto)
                                        <tr>
                                            <td>{{ $produto->codigo }}</td>
                                            <td>{{ $produto->basico }}</td>
                                            <td>{{ $produto->marca }}</td>
                                            <td> @for($i=0; $i <= $produto->ranking; $i++ ) <i class="fa fa-star text-warning" aria-hidden="true"></i> @endfor  </td>
                                            <td>{{ substr($produto->descr1, 0, 45 ) }}</td>
                                            <td>R$ {{ number_format($produto->prvist, 2, ',', '.') }}</td>
                                            <td> @isset($produto->estoque()->where('empfil',$logged_in_user->empresa->codigo)->latest('dtmovi')->first()->qtatua)
                                                    {{number_format($produto->estoque()->where('empfil',$logged_in_user->empresa->codigo)->latest('dtmovi')->first()->qtatua, 0, ',', '.')  }}
                                                @else 0
                                                      @endisset
                                            </td>

                                            <td>{{ date('d-m-Y', strtotime($produto->data_atualizacao)) }}</td>
                                            <td>{!!   $produto->envia_site == true ? '<i class="fa fa-check text-success" aria-hidden="true">': ''  !!}</td>
                                            <td>

                                                <div class="btn-group btn-group-sm" role="group" aria-label="Sincronizar">
                                                <a href="{{ route('admin.atualizaProdutoMarktplace', $produto->codigo) }}" class="btn btn-info">
                                                        <i class="fa fa-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="Sincronizar"></i></a>
                                                    <a href="{{route('admin.SSProdutosEditar',$produto->codigo)}}" class="btn btn-primary">
                                                        <i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar"></i></a>
                                                    <a class="btn btn-primary" data-toggle="collapse" data-target=".multi-collapse{{$produto->similarm}}" aria-expanded="false" aria-controls="multiCollapse{{$produto->similarm}}" >
                                                        <i class="fa fa-sort-desc" data-toggle="tooltip" data-placement="top" title="Similares" data-original-title="Similar"></i></a>
                                                </div>

                                            </td>

                                        </tr>


                                        @foreach($produto->similares as $similar)

                                        <tr class="table-sm collapse multi-collapse{{$produto->similarm}}" id="multiCollapse{{$produto->similarm}}">

                                            <td>{{ $similar->codigo }}</td>
                                            <td>{{ $similar->basico }}</td>
                                            <td>{{ $similar->marca }}</td>
                                            <td> @for($i=0; $i <= $similar->ranking; $i++ ) <i class="fa fa-star text-warning" aria-hidden="true"></i> @endfor  </td>
                                            <td>{{ substr($similar->descr1, 0, 45 ) }} <span class="badge badge-secondary">Similar</span></td>
                                            <td>R$ {{ number_format($similar->prvist, 2, ',', '.') }}</td>
                                            <td> @isset($similar->estoque()->where('empfil',$logged_in_user->empresa->codigo)->latest('dtmovi')->first()->qtatua)
                                                    {{number_format( $similar->estoque()->where('empfil',$logged_in_user->empresa->codigo)->latest('dtmovi')->first()->qtatua, 0, ',', '.')  }}
                                                @else 0
                                                @endisset
                                            </td>
                                            <td>{{ date('d-m-Y', strtotime($similar->data_atualizacao)) }}</td>
                                            <td>

                                                <div class="btn-group btn-group-sm" role="group" aria-label="User Actions">
                                                    <a href="http://homestead.app/admin/auth/user/1" class="btn btn-info">
                                                        <i class="fa fa-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="Sincronizar"></i></a>
                                                    <a href="{{route('admin.SSProdutosEditar',$similar->codigo)}}" class="btn btn-primary">
                                                        <i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar"></i></a>
                                                </div>

                                            </td>
                                        </tr>

                                            @endforeach
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endisset
                </div><!--col-->
            </div><!--row-->
            <div class="row">
                <div class="col-7">

                    <div class="float-left">

                    </div>
                </div><!--col-->

                <div class="col-5">
                    <div class="float-right">
                        @isset($produtos)
                        {{ $produtos->appends(['search' => isset($search) ? $search : ''])->links() }}
                            @endisset
                    </div>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
    </div><!--card-->
@endsection