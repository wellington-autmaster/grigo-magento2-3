<tr>
    <td>{{ $produto->codigo_interno }}</td>
    <td>{{ $produto->codigo_fabricante }}</td>
    <td>{{ substr($produto->descricao, 0, 45 ) }}</td>
    <td>R$ {{ number_format($produto->preco_venda, 2, ',', '.') }}</td>
    <td>{{ number_format($produto->estoque_disponivel, 0, ',', '.') }}</td>
    <td>{{ $produto->data_atualizacao }}</td>
    <td>

        <div class="btn-group btn-group-sm" role="group" aria-label="User Actions">
            <a href="http://homestead.app/admin/auth/user/1" class="btn btn-info">
                <i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"></i></a>
            <a href="#" class="btn btn-primary">
                <i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar"></i></a>
        </div>

    </td>