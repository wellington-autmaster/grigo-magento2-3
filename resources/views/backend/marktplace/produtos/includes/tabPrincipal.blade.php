<div class="form-group row">
    <label for="codigoInterno" class="col-sm-2 col-form-label">Código interno SSPlus</label>
    <div class="col-sm-2">
        <input name="codigoInterno" id="codigoInterno" readonly type="text"  class="form-control"  value="{{$produto->codigo}}">
    </div>
    <label for="codigoFabricante" class="col-sm-2 col-form-label">Código do Fabricante</label>
    <div class="col-sm-2">
        <input name="codigoFabricante" id="codigoFabricante" type="text" readonly class="form-control"  value="{{$produto->basico}}">
    </div>
    <label for="codigoBarras" class="col-sm-2 col-form-label">Código de Barras</label>
    <div class="col-sm-2">
        <input name="codigoBarras" id="codigoBarras" readonly type="text"  class="form-control"  value="{{$produto->codire}}">
    </div>
</div>
<div class="form-group row">
    <label for="nomeProduto" class="col-sm-2 col-form-label">Nome do produto</label>
    <div class="col-sm-10">
        <input name="nomeProduto" id="nomeProduto" type="text" class="form-control" placeholder="{{$produto->descr1}}">
    </div>
</div>
<div class="form-group row">
    <label for="marca" class="col-sm-2 col-form-label">Marca</label>
    <div class="col-sm-10">
        <input name="marca" id="marca" type="text" class="form-control" placeholder="Marca" value="{{$produto->marca}}">
    </div>
</div>
<div class="form-group row">
    <label for="precoVista" class="col-sm-2 col-form-label">Preço de Venda</label>
    <div class="col-sm-4">
        <input name="precoVista" id="precoVista" type="text" class="form-control" placeholder="Preço de venda" value="{{ number_format($produto->prvist, 2, ',', '.') }}">
    </div>
    <label for="estoqueDisponivel" class="col-sm-2 col-form-label">Estoque Disponível</label>
    <div class="col-sm-4">
        <input name="estoqueDisponivel" id="estoqueDisponivel" type="text" class="form-control"
               value="@isset($produto->estoque()->where('empfil',$logged_in_user->empresa->codigo)->latest('dtmovi')->first()->qtatua)
               {{number_format( $produto->estoque()->where('empfil',$logged_in_user->empresa->codigo)->latest('dtmovi')->first()->qtatua, 0, ',', '.')  }}
               @else 0
               @endisset">
    </div>
</div>


