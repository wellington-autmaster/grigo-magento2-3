@extends('backend.layouts.app')

@section ('title', 'Editar  produtos')

@section('breadcrumb-links')
    @include('backend.marktplace.produtos.includes.breadcrumb-links')
@endsection


@section('content')
    <div id="app" class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-3">
                    <h4 class="card-title mb-0">
                        Informações do produto
                        <small class="text-muted"> SSPlus</small>

                    </h4>
                </div><!--col-->

            </div><!--row-->

            <div class="row mt-4">
                <div class="col">
                    <form id="frmPesquisaProduto" action="{{ route('admin.SSprodutosPesquisa') }}" method="get">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="principal-tab" data-toggle="tab" href="#principal" role="tab" aria-controls="principal" aria-selected="true">Principal</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Caracteristicas</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="veiculos-tab" data-toggle="tab" href="#veiculos" role="tab" aria-controls="veiculos" aria-selected="false">Veiculos</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="principal" role="tabpanel"
                                 aria-labelledby="principal-tab">@include('backend.marktplace.produtos.includes.tabPrincipal')</div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">@include('backend.marktplace.produtos.includes.tabProdutoCaracteristicas')</div>
                            <div class="tab-pane fade" id="veiculos" role="tabpanel" aria-labelledby="veiculos-tab">@include('backend.marktplace.produtos.includes.tabVeiculos')</div>
                        </div>
                    </form>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
    </div><!--card-->
@endsection