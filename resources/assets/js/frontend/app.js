
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('../bootstrap');
require('../plugins');
import VueResource from 'vue-resource';



window.Vue = require('vue');
Vue.use(VueResource);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/ExampleComponent.vue'));
Vue.component('vc-header', require('./components/monitor/Header.vue').default);
Vue.component('vc-senhas-aguardando', require('./components/monitor/SenhasAguardando').default);
Vue.component('vc-senhas-processamento', require('./components/monitor/SenhasProcessamento').default);
Vue.component('vc-senhas-concluido', require('./components/monitor/SenhasConcluido').default);
Vue.component('promocoes', require('./components/monitor/CarouselPromocoes.vue').default);
Vue.component('vc-noticias', require('./components/monitor/Noticias.vue').default);
Vue.component('vc-noticias-footer', require('./components/monitor/NoticiasFooter.vue').default);

Vue.component('vc-ordem-servico', require('./components/monitorordemservico/Ordens.vue').default);
Vue.component('vc-notify', require('./components/monitor/pg_notify').default);

//Monitor status pedido (spss - Status pedidos ss)

//headers
Vue.component('vc-container', require('./components/MonitorHistoricoPedidoSS/vc-container').default);
Vue.component('vc-spss-header', require('./components/MonitorHistoricoPedidoSS/header').default);
Vue.component('vc-spss-totalpedidos', require('./components/MonitorHistoricoPedidoSS/header/header-total-pedidos').default);
Vue.component('vc-spss-separacao', require('./components/MonitorHistoricoPedidoSS/header/header-separacao').default);
Vue.component('vc-spss-conferencia', require('./components/MonitorHistoricoPedidoSS/header/header-conferencia').default);
Vue.component('vc-spss-entrega', require('./components/MonitorHistoricoPedidoSS/header/header-entrega').default);
Vue.component('vc-spss-totalentrega', require('./components/MonitorHistoricoPedidoSS/header/header-totalentrega').default);
Vue.component('vc-spss-datatable', require('./components/MonitorHistoricoPedidoSS/vc-datatable').default);
Vue.component('vc-spss-modal', require('./components/MonitorHistoricoPedidoSS/vc-modal-table').default);

Vue.component('vc-spss-pedidos', require('./components/MonitorHistoricoPedidoSS/vc-pedidos-ss').default);
Vue.component('vc-spss-pedido', require('./components/MonitorHistoricoPedidoSS/vc-pedido-ss').default);


const app = new Vue({
    el: '#app'
});
